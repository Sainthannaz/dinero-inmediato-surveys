package mx.com.dineroinmediato.surveys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mx.com.dineroinmediato.surveys.R;
import mx.com.dineroinmediato.surveys.models.Surveys;
import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;

public class ViewExitSurveyActivity extends AppCompatActivity {
    DatabaseHandler db;
    Button cancel;
    Integer idSurvey;
    String usuario, sucursal, cliente, colonia, oficio, genero, edad, p1, p2, p3 , p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, email, fecha, timer;

    TextView movimientot, coloniat, oficiot, generot, edadt, p1t, p2t, p3t, p4t, p5t, p6t, p7t, p8t, p9t, p10t, p11t, p12t, p13t, p14t, p15t, p16t, emailt;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_exit_survey);
        cancel = findViewById(R.id.button_cancelar);
        movimientot = findViewById(R.id.movimiento);
        coloniat = findViewById(R.id.colonia);
        oficiot = findViewById(R.id.oficio);
        generot = findViewById(R.id.genero);
        edadt = findViewById(R.id.edad);
        p1t = findViewById(R.id.p1);
        p2t = findViewById(R.id.p2);
        p3t = findViewById(R.id.p3);
        p4t = findViewById(R.id.p4);
        p5t = findViewById(R.id.p5);
        p6t = findViewById(R.id.p6);
        p7t = findViewById(R.id.p7);
        p8t = findViewById(R.id.p8);
        p9t = findViewById(R.id.p9);
        p10t = findViewById(R.id.p10);
        p11t = findViewById(R.id.p11);
        p12t = findViewById(R.id.p12);
        p13t = findViewById(R.id.p13);
        p14t = findViewById(R.id.p14);
        p15t = findViewById(R.id.p15);
        p16t = findViewById(R.id.p16);
        emailt = findViewById(R.id.email);

        db = new DatabaseHandler(getApplicationContext());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idSurvey = extras.getInt("idSurvey");
            /*
            usuario = extras.getString("usuario");
            sucursal = extras.getString("sucursal");
            cliente = extras.getString("cliente");
            colonia = extras.getString("colonia");
            oficio = extras.getString("oficio");
            genero = extras.getString("genero");
            edad = extras.getString("edad");
            p1 = extras.getString("p1");
            p2 = extras.getString("p2");
            p3 = extras.getString("p3");
            p4 = extras.getString("p4");
            p5 = extras.getString("p5");
            p6 = extras.getString("p6");
            p7 = extras.getString("p7");
            p8 = extras.getString("p8");
            p9 = extras.getString("p9");
            p10 = extras.getString("p10");
            p11 = extras.getString("p11");
            p12 = extras.getString("p12");
            p13 = extras.getString("p13");
            p14 = extras.getString("p14");
            p15 = extras.getString("p15");
            p16 = extras.getString("p16");
            email = extras.getString("email");
            fecha = extras.getString("fecha");
            timer = extras.getString("timer");
            System.out.println("Survey ID: " + idSurvey);
            System.out.println("usuario: " + usuario);
            System.out.println("cliente: " + cliente);
            System.out.println("colonia: " + colonia);
            System.out.println("genero: " + genero);*/
        } else {
            System.out.println("¡No pasaron datos o no existen!");
        }

        Surveys surveydata = db.getInfoSurvey(idSurvey);
        System.out.println("Mostrando datos de la consulta");
        System.out.println(surveydata.getsurvey_cliente());
        System.out.println(surveydata.getsurvey_colonia());
        movimientot.setText(surveydata.getsurvey_cliente());
        coloniat.setText(surveydata.getsurvey_colonia());
        oficiot.setText(surveydata.getsurvey_oficio());
        generot.setText(surveydata.getsurvey_genero());
        edadt.setText(surveydata.getsurvey_edad());
        p1t.setText(surveydata.getsurvey_p1());
        p2t.setText(surveydata.getsurvey_p2());
        p3t.setText(surveydata.getsurvey_p3());
        p4t.setText(surveydata.getsurvey_p4());
        p5t.setText(surveydata.getsurvey_p5());
        p6t.setText(surveydata.getsurvey_p6());
        p7t.setText(surveydata.getsurvey_p7());
        p8t.setText(surveydata.getsurvey_p8());
        p9t.setText(surveydata.getsurvey_p9());
        p10t.setText(surveydata.getsurvey_p10());
        p11t.setText(surveydata.getsurvey_p11());
        p12t.setText(surveydata.getsurvey_p12());
        p13t.setText(surveydata.getsurvey_p13());
        p14t.setText(surveydata.getsurvey_p14());
        p15t.setText(surveydata.getsurvey_p15());
        p16t.setText(surveydata.getsurvey_p16());
        emailt.setText(surveydata.getsurvey_email());
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }
}
