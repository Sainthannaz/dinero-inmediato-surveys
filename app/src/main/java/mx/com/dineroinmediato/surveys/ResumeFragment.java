package mx.com.dineroinmediato.surveys;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;
import mx.com.dineroinmediato.surveys.tools.SessionManager;

public class ResumeFragment extends Fragment {
    private ProgressDialog pDialog;
    SessionManager session;
    TextView user, branch;
    String userEmail, userBranch;
    ImageButton btn_salida, btn_top;
    Long resume_exit, resume_top;
    TextView resumen_salida, resumen_top;
    DatabaseHandler db;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_resume,null);
        user = view.findViewById(R.id.user);
        branch = view.findViewById(R.id.branch);
        resumen_salida = view.findViewById(R.id.resume_salida);
        resumen_top = view.findViewById(R.id.resume_top);
        btn_salida = view.findViewById(R.id.btn_esalida);
        btn_top = view.findViewById(R.id.btn_etop);
        session = new SessionManager(getContext());
        HashMap<String, String> userSession = session.getUserDetails();
        db = new DatabaseHandler(getContext());
        userEmail = userSession.get(SessionManager.KEY_EMAIL);
        HashMap<String, String> sucursal = session.getUserSucursal();
        userBranch = sucursal.get(SessionManager.KEY_SUCURSAL);
        user.setText(userEmail);
        branch.setText(userBranch);
        btn_salida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSurvey(view);
            }
        });
        btn_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSurveyTopOfMind(view);
            }
        });
        generateResume();
        return view;

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void launchSurvey(View view){
        Intent intent = new Intent(getActivity(), SurveySalidaActivity.class);
        startActivity(intent);
    }

    private void launchSurveyTopOfMind(View view){
        Intent intent = new Intent(getActivity(), SurveyTopMindActivity.class);
        startActivity(intent);
    }

    private void generateResume(){
        resume_exit = db.getExitSurveyCount();
        resume_top = db.getTopSurveyCount();
        if (resume_exit == 0 ){
            System.out.println("Valor es 0");
            resumen_salida.setText("0");
        } else {
            System.out.println("Hay un valor");
            resumen_salida.setText(String.valueOf(resume_exit));
        }
        if (resume_top == 0 ){
            System.out.println("Valor es 0");
            resumen_top.setText("0");
        } else {
            System.out.println("Hay un valor");
            resumen_top.setText(String.valueOf(resume_top));
        }

    }
}
