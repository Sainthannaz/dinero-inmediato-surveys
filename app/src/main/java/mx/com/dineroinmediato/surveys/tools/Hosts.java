package mx.com.dineroinmediato.surveys.tools;


/**
 * Created by Gabriel on 03/01/2018.
 */

public class Hosts {
    /**
     * Transición Home -> Detalle
     */
    public static final int CODIGO_DETALLE = 100;
    /**
     * Transición Detalle -> Actualización
     */
    public static final int CODIGO_ACTUALIZACION = 101;
    /**
     * Puerto que utilizas para la conexión.
     * Dejalo en blanco si no has configurado esta carácteristica.
     */
    private static final String PUERTO_HOST = "";
    /**
     * Dirección IP de genymotion o AVD
     */
    //private static final String IP = "ventasdi.no-ip.net";
    private static final String IP = "rebelbot.mx";
    //private static final String IP =  "192.168.0.10";
    /**
     * URLs del Web Service
     */

    //public static final String GET_LOGIN = "https://" + IP + PUERTO_HOST + "WebAPIEncuestas/api/Seguridad/validarCredenciales";
    public static final String GET_LOGIN = "https://" + IP + PUERTO_HOST + "/dineroinmediato/movil/login.php";
    public static final String TEST = "http://ventasdi.no-ip.net/WebAPIEncuestas/api/TestConexionWS/testConexion";
    /**
     * Clave para el valor extra que representa al identificador de una meta
     */
    public static final String EXTRA_ID = "IDEXTRA";

}
