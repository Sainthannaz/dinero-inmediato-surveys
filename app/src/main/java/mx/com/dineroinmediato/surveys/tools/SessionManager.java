package mx.com.dineroinmediato.surveys.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import java.util.HashMap;

public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;


    Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "DineroInmediato";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private String KEY_IP = null;

    // Uid
    public static final String KEY_UID ="uid";
    // Name
    public static final String KEY_STATUS ="status";
    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    public static final String KEY_SUCURSAL = "sucursal";


    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "Sesion de usuario modificada!");
    }

    public void createLoginSession(boolean isLoggedIn, String uid, String status, String email){
        // Storing login value as TRUE
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        editor.putString(KEY_UID, uid);
        //Storing name in pref
        editor.putString(KEY_STATUS, status);
        // Storing email in pref
        editor.putString(KEY_EMAIL, email);
        // commit changes
        editor.commit();
    }

    public void setSucursal(String sucursal){
        editor.putString(KEY_SUCURSAL, sucursal);
        editor.commit();
    }

    public HashMap<String, String> getUserSucursal() {
        HashMap<String, String> sucursal = new HashMap<String, String>();
        sucursal.put(KEY_SUCURSAL, pref.getString(KEY_SUCURSAL, null));
        return sucursal;
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_UID, pref.getString(KEY_UID, null));
        // user name
        user.put(KEY_STATUS, pref.getString(KEY_STATUS, null));
        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        // return user
        return user;
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}

