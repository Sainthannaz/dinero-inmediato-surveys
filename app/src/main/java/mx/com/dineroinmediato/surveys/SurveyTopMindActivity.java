package mx.com.dineroinmediato.surveys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;
import mx.com.dineroinmediato.surveys.tools.SessionManager;

public class SurveyTopMindActivity extends Activity implements
        AdapterView.OnItemSelectedListener {
    Button btnCancelar, btnGuardar;
    FloatingActionButton fab;
    private SessionManager session;
    Date date;
    String userEmail, userSucursal;
    private ProgressDialog pDialog;
    Spinner branchSpinner, colSpinner;
    DatabaseHandler db;
    Chronometer simpleChronometer;
    View scrollView;
    EditText colonia, oficio, otro, email, qst1_1, qst1_2, qst1_3, otro_qst2, pr3, otro_qst3, otro_qst5, pr4_1, pr4_2, pr4_3,
            pr5_1_1, pr5_1_2, pr5_2_1, pr5_2_2, pr5_3_1, pr5_3_2, pr5_4_1, pr5_4_2, pr5_5_1, pr5_5_2, pr6;
    RadioGroup sexo, edad, pr2;
    RadioButton masculino, femenino, edad1, edad2, edad3, edad4, radio_2_1, radio_2_2;
    String cli, colony, col, ofi, gen, eda, p1_1, p1_2, p1_3, ps1_1_1, ps1_1_2, ps1_1_3, ps1_1_4, ps1_2_1, ps1_2_2, ps1_2_3, ps1_2_4, ps1_3_1, ps1_3_2, ps1_3_3, ps1_3_4, p2, p3, p4_1, p4_2, p4_3, p5_1_1_1, p5_1_1_2, p5_2_1_1, p5_2_1_2, p5_3_1_1, p5_3_1_2, p5_4_1_1, p5_4_1_2, p5_5_1_1, p5_5_1_2, p6, selectedBranch;
    CheckBox chck_1_1, chck_1_2, chck_1_3, chck_1_4, chck_2_1, chck_2_2, chck_2_3, chck_2_4,
            chck_3_1, chck_3_2, chck_3_3, chck_3_4, chck_5_1, chck_5_2, chck_5_3, chck_15_1, chck_15_2, chck_15_3, chck_15_4, chck_15_5;
    /* Arreglos donde se concatenaba */
    //ArrayList<String> pregunta1_1 = new ArrayList<>();
    //ArrayList<String> pregunta1_2 = new ArrayList<>();
    //ArrayList<String> pregunta1_3 = new ArrayList<>();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        setContentView(R.layout.survey_topmind_survey);
        simpleChronometer = (Chronometer) findViewById(R.id.simpleChronometer);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.shake);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        animation.setRepeatCount(Animation.INFINITE);
        fab.startAnimation(animation);
        date = new Date();
        db = new DatabaseHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> sucursal = session.getUserSucursal();
        userSucursal = sucursal.get(SessionManager.KEY_SUCURSAL);
        HashMap<String, String> user = session.getUserDetails();
        userEmail = user.get(SessionManager.KEY_EMAIL);
        btnCancelar = (Button) findViewById(R.id.button_cancelar);
        btnGuardar = (Button) findViewById(R.id.button_finalizar);

        simpleChronometer = (Chronometer) findViewById(R.id.simpleChronometer);

        branchSpinner = (Spinner) findViewById(R.id.spSucursales);
        colSpinner = (Spinner) findViewById(R.id.colonias);
        branchSpinner.setOnItemSelectedListener(this);
        loadSpinnerDataBranchs();
        //loadSpinnerDataColonies();
        String formatType = simpleChronometer.getFormat();
        // Dialogo de proceso
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        colonia = findViewById(R.id.colonia);
        oficio = findViewById(R.id.oficio);
        colonia.setEnabled(false);
        oficio.setEnabled(false);

        sexo = findViewById(R.id.sexo);
        masculino = findViewById(R.id.masculino);
        femenino = findViewById(R.id.femenino);
        femenino.setEnabled(false);
        masculino.setEnabled(false);

        edad = findViewById(R.id.edad);
        edad1 = findViewById(R.id.edad1);
        edad2 = findViewById(R.id.edad2);
        edad3 = findViewById(R.id.edad3);
        edad4 = findViewById(R.id.edad4);
        edad1.setEnabled(false);
        edad2.setEnabled(false);
        edad3.setEnabled(false);
        edad4.setEnabled(false);

        qst1_1 = findViewById(R.id.otro_qst1_1);
        qst1_2 = findViewById(R.id.otro_qst1_2);
        qst1_3 = findViewById(R.id.otro_qst1_3);
        qst1_1.setEnabled(false);
        qst1_2.setEnabled(false);
        qst1_3.setEnabled(false);

        chck_1_1 = findViewById(R.id.chck_1_1);
        chck_1_2 = findViewById(R.id.chck_1_2);
        chck_1_3 = findViewById(R.id.chck_1_3);
        chck_1_4 = findViewById(R.id.chck_1_4);
        chck_1_1.setEnabled(false);
        chck_1_2.setEnabled(false);
        chck_1_3.setEnabled(false);
        chck_1_4.setEnabled(false);

        chck_2_1 = findViewById(R.id.chck_2_1);
        chck_2_2 = findViewById(R.id.chck_2_2);
        chck_2_3 = findViewById(R.id.chck_2_3);
        chck_2_4 = findViewById(R.id.chck_2_4);
        chck_2_1.setEnabled(false);
        chck_2_2.setEnabled(false);
        chck_2_3.setEnabled(false);
        chck_2_4.setEnabled(false);


        chck_3_1 = findViewById(R.id.chck_3_1);
        chck_3_2 = findViewById(R.id.chck_3_2);
        chck_3_3 = findViewById(R.id.chck_3_3);
        chck_3_4 = findViewById(R.id.chck_3_4);
        chck_3_1.setEnabled(false);
        chck_3_2.setEnabled(false);
        chck_3_3.setEnabled(false);
        chck_3_4.setEnabled(false);


        pr2 = findViewById(R.id.qst2);
        radio_2_1 = findViewById(R.id.radio_2_1);
        radio_2_2 = findViewById(R.id.radio_2_2);
        otro_qst2 = findViewById(R.id.otro_qst2);
        radio_2_1.setEnabled(false);
        radio_2_2.setEnabled(false);
        otro_qst2.setEnabled(false);

        pr3 = findViewById(R.id.qst3);
        pr3.setEnabled(false);

        pr4_1 = findViewById(R.id.pr4_1);
        pr4_2 = findViewById(R.id.pr4_2);
        pr4_3 = findViewById(R.id.pr4_3);
        pr4_1.setEnabled(false);
        pr4_2.setEnabled(false);
        pr4_3.setEnabled(false);
        pr5_1_1 = findViewById(R.id.pr5_1_1);
        pr5_1_2 = findViewById(R.id.pr5_1_2);
        pr5_2_1 = findViewById(R.id.pr5_2_1);
        pr5_2_2 = findViewById(R.id.pr5_2_2);
        pr5_3_1 = findViewById(R.id.pr5_3_1);
        pr5_3_2 = findViewById(R.id.pr5_3_2);
        pr5_4_1 = findViewById(R.id.pr5_4_1);
        pr5_4_2 = findViewById(R.id.pr5_4_2);
        pr5_5_1 = findViewById(R.id.pr5_5_1);
        pr5_5_2 = findViewById(R.id.pr5_5_2);
        pr5_1_1.setEnabled(false);
        pr5_1_2.setEnabled(false);
        pr5_2_1.setEnabled(false);
        pr5_2_2.setEnabled(false);
        pr5_3_1.setEnabled(false);
        pr5_3_2.setEnabled(false);
        pr5_4_1.setEnabled(false);
        pr5_4_2.setEnabled(false);
        pr5_5_1.setEnabled(false);
        pr5_5_2.setEnabled(false);

        pr6 = findViewById(R.id.qst6);
        pr6.setEnabled(false);

        sexo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.masculino:
                        gen = "masculino";
                        break;
                    case R.id.femenino:
                        gen = "femenino";
                        break;
                }
            }
        });

        qst1_1.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String checkQst1;
                checkQst1 = qst1_1.getText().toString();
                if (checkQst1 != null && !checkQst1.isEmpty() && !checkQst1.equals("null")){
                    chck_1_1.setEnabled(true);
                    chck_1_2.setEnabled(true);
                    chck_1_3.setEnabled(true);
                    chck_1_4.setEnabled(true);
                } else {
                    chck_1_1.setEnabled(false);
                    chck_1_2.setEnabled(false);
                    chck_1_3.setEnabled(false);
                    chck_1_4.setEnabled(false);
                    chck_1_1.setChecked(false);
                    chck_1_2.setChecked(false);
                    chck_1_3.setChecked(false);
                    chck_1_4.setChecked(false);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        qst1_2.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String checkQst2;
                checkQst2 = qst1_2.getText().toString();
                if (checkQst2 != null && !checkQst2.isEmpty() && !checkQst2.equals("null")){
                    chck_2_1.setEnabled(true);
                    chck_2_2.setEnabled(true);
                    chck_2_3.setEnabled(true);
                    chck_2_4.setEnabled(true);
                } else {
                    chck_2_1.setEnabled(false);
                    chck_2_2.setEnabled(false);
                    chck_2_3.setEnabled(false);
                    chck_2_4.setEnabled(false);
                    chck_2_1.setChecked(false);
                    chck_2_2.setChecked(false);
                    chck_2_3.setChecked(false);
                    chck_2_4.setChecked(false);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        qst1_3.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String checkQst3;
                checkQst3 = qst1_3.getText().toString();
                if (checkQst3 != null && !checkQst3.isEmpty() && !checkQst3.equals("null")){
                    chck_3_1.setEnabled(true);
                    chck_3_2.setEnabled(true);
                    chck_3_3.setEnabled(true);
                    chck_3_4.setEnabled(true);
                } else {
                    chck_3_1.setEnabled(false);
                    chck_3_2.setEnabled(false);
                    chck_3_3.setEnabled(false);
                    chck_3_4.setEnabled(false);
                    chck_3_1.setChecked(false);
                    chck_3_2.setChecked(false);
                    chck_3_3.setChecked(false);
                    chck_3_4.setChecked(false);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        edad.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.edad1:
                        eda = "18 a 24";
                        break;
                    case R.id.edad2:
                        eda = "25 a 39";
                        break;
                    case R.id.edad3:
                        eda = "40 a 64";
                        break;
                    case R.id.edad4:
                        eda = "65 y mas";
                        break;
                }
            }
        });


        pr2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_2_1:

                        if (otro_qst2.isEnabled()) {
                            otro_qst2.setEnabled(false);
                        } else {
                            otro_qst2.setEnabled(true);
                            otro_qst2.requestFocus();
                        }
                        break;
                    case R.id.radio_2_2:
                        p2 = "No";
                        otro_qst2.setText("");
                        if (otro_qst2.isEnabled()) {
                            otro_qst2.setEnabled(false);
                            otro_qst2.clearFocus();
                        } else {
                            otro_qst2.setEnabled(false);
                            otro_qst2.clearFocus();
                        }
                        break;
                }
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String branch = branchSpinner.getSelectedItem().toString();
                colony = colSpinner.getSelectedItem().toString();

                if(!branch.equals("SUCURSALES") && !colony.equals("SUCURSALES")){
                    Toast.makeText(getApplicationContext(),"Has seleccionado " + branch , Toast.LENGTH_SHORT).show();
                    session.setSucursal(branch);
                    HashMap<String, String> sucursal = session.getUserSucursal();
                    userSucursal = sucursal.get(SessionManager.KEY_SUCURSAL);
                    activateSurvey();
                    Toast.makeText(getApplicationContext(), "Inicia la encuesta!",
                            Toast.LENGTH_LONG).show();
                    simpleChronometer.setBase(SystemClock.elapsedRealtime());
                    simpleChronometer.start();
                }else {
                    Snackbar.make(view, "Debes de Seleccionar una sucursal y una colonia", Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onExitSurvey();
            }
        });


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                getData();
            }
        });
    }

    public void activateSurvey() {
        fab.setEnabled(false);
        branchSpinner.setEnabled(false);
        colSpinner.setEnabled(false);
        colonia.setEnabled(true);
        oficio.setEnabled(true);
        femenino.setEnabled(true);
        masculino.setEnabled(true);
        edad1.setEnabled(true);
        edad2.setEnabled(true);
        edad3.setEnabled(true);
        edad4.setEnabled(true);
        qst1_1.setEnabled(true);
        qst1_2.setEnabled(true);
        qst1_3.setEnabled(true);
        /*chck_1_1.setEnabled(true);
        chck_1_2.setEnabled(true);
        chck_1_3.setEnabled(true);
        chck_1_4.setEnabled(true);
        chck_2_1.setEnabled(true);
        chck_2_2.setEnabled(true);
        chck_2_3.setEnabled(true);
        chck_2_4.setEnabled(true);
        chck_3_1.setEnabled(true);
        chck_3_2.setEnabled(true);
        chck_3_3.setEnabled(true);
        chck_3_4.setEnabled(true);*/
        radio_2_1.setEnabled(true);
        radio_2_2.setEnabled(true);
        pr3.setEnabled(true);
        pr4_1.setEnabled(true);
        pr4_2.setEnabled(true);
        pr4_3.setEnabled(true);
        pr5_1_1.setEnabled(true);
        pr5_1_2.setEnabled(true);
        pr5_2_1.setEnabled(true);
        pr5_2_2.setEnabled(true);
        pr5_3_1.setEnabled(true);
        pr5_3_2.setEnabled(true);
        pr5_4_1.setEnabled(true);
        pr5_4_2.setEnabled(true);
        pr5_5_1.setEnabled(true);
        pr5_5_2.setEnabled(true);
        pr6.setEnabled(true);
        btnGuardar.setEnabled(true);
        branchSpinner.setEnabled(false);
    }


    public void disableSurvey() {
        fab.setEnabled(true);
        branchSpinner.setEnabled(true);
        colSpinner.setEnabled(true);
        colonia.setEnabled(false);
        colonia.setText("");
        oficio.setEnabled(false);
        oficio.setText("");
        femenino.setEnabled(false);
        masculino.setEnabled(false);
        sexo.clearCheck();
        edad1.setEnabled(false);
        edad2.setEnabled(false);
        edad3.setEnabled(false);
        edad4.setEnabled(false);
        edad.clearCheck();
        qst1_1.setEnabled(false);
        qst1_1.setText("");
        qst1_2.setEnabled(false);
        qst1_2.setText("");
        qst1_3.setEnabled(false);
        qst1_3.setText("");
        chck_1_1.setEnabled(false);
        chck_1_2.setEnabled(false);
        chck_1_3.setEnabled(false);
        chck_1_4.setEnabled(false);
        chck_2_1.setEnabled(false);
        chck_2_2.setEnabled(false);
        chck_2_3.setEnabled(false);
        chck_2_4.setEnabled(false);
        chck_3_1.setEnabled(false);
        chck_3_2.setEnabled(false);
        chck_3_3.setEnabled(false);
        chck_3_4.setEnabled(false);
        chck_1_1.setChecked(false);
        chck_1_2.setChecked(false);
        chck_1_3.setChecked(false);
        chck_1_4.setChecked(false);
        chck_2_1.setChecked(false);
        chck_2_2.setChecked(false);
        chck_2_3.setChecked(false);
        chck_2_4.setChecked(false);
        chck_3_1.setChecked(false);
        chck_3_2.setChecked(false);
        chck_3_3.setChecked(false);
        chck_3_4.setChecked(false);
        pr2.clearCheck();
        radio_2_1.setEnabled(false);
        radio_2_2.setEnabled(false);
        otro_qst2.setText("");
        otro_qst2.setEnabled(false);
        pr3.setEnabled(false);
        pr3.setText("");
        pr4_1.setEnabled(false);
        pr4_2.setEnabled(false);
        pr4_3.setEnabled(false);
        pr4_1.setText("");
        pr4_2.setText("");
        pr4_3.setText("");
        pr5_1_1.setEnabled(false);
        pr5_1_2.setEnabled(false);
        pr5_2_1.setEnabled(false);
        pr5_2_2.setEnabled(false);
        pr5_3_1.setEnabled(false);
        pr5_3_2.setEnabled(false);
        pr5_4_1.setEnabled(false);
        pr5_4_2.setEnabled(false);
        pr5_5_1.setEnabled(false);
        pr5_5_2.setEnabled(false);
        pr5_1_1.setText("");
        pr5_1_2.setText("");
        pr5_2_1.setText("");
        pr5_2_2.setText("");
        pr5_3_1.setText("");
        pr5_3_2.setText("");
        pr5_4_1.setText("");
        pr5_4_2.setText("");
        pr5_5_1.setText("");
        pr5_5_2.setText("");
        pr6.setEnabled(false);
        pr6.setText("");
        btnGuardar.setEnabled(false);
        branchSpinner.setEnabled(true);
    }

    private void checkData(){
        System.out.println("Entramos a checar...............................>>>>>>>>>>>>>>>>>>");

        if(chck_1_1.isChecked()){
            ps1_1_1 = "Imagen";
        } else {
            ps1_1_1 = "";
        }
        if(chck_1_2.isChecked()){
            ps1_1_2 = "Color";
        } else {
            ps1_1_2 = "";
        }
        if(chck_1_3.isChecked()){
            ps1_1_3 = "Ubicación";
        } else {
            ps1_1_3 = "";
        }
        if(chck_1_4.isChecked()){
            ps1_1_4 = "Nombre";
        } else {
            ps1_1_4 = "";
        }
        System.out.println("*********************DATOS DE LOS CHECK **********************************");

        System.out.println(ps1_1_1);
        System.out.println(ps1_1_2);
        System.out.println(ps1_1_3);
        System.out.println(ps1_1_4);

        if(chck_2_1.isChecked()){
            ps1_2_1 = "Imagen";
        } else {
            ps1_2_1 = "";
        }
        if(chck_2_2.isChecked()){
            ps1_2_2 = "Color";
        } else {
            ps1_2_2 = "";
        }
        if(chck_2_3.isChecked()){
            ps1_2_3 = "Ubicación";
        } else {
            ps1_2_3 = "";
        }
        if(chck_2_4.isChecked()){
            ps1_2_4 = "Nombre";
        } else {
            ps1_2_4 = "";
        }
        System.out.println(ps1_2_1);
        System.out.println(ps1_2_2);
        System.out.println(ps1_2_3);
        System.out.println(ps1_2_4);


        if(chck_3_1.isChecked()){
            ps1_3_1 = "Imagen";
        } else {
            ps1_3_1 = "";
        }
        if(chck_3_2.isChecked()){
            ps1_3_2 = "Color";
        } else {
            ps1_3_2 = "";
        }
        if(chck_3_3.isChecked()){
            ps1_3_3 = "Ubicación";
        } else {
            ps1_3_3 = "";
        }
        if(chck_3_4.isChecked()){
            ps1_3_4 = "Nombre";
        } else {
            ps1_3_4 = "";
        }

        System.out.println(ps1_3_1);
        System.out.println(ps1_3_2);
        System.out.println(ps1_3_3);
        System.out.println(ps1_3_4);


        if(radio_2_1.isChecked()){
            p2 = (otro_qst2.getText().toString());
        }

        p1_1 = qst1_1.getText().toString();
        p1_2 = qst1_2.getText().toString();
        p1_3 = qst1_3.getText().toString();

        /*
        p1_1 = TextUtils.join(", ", pregunta1_1);
        System.out.println(qst1_1.getText().toString());
        System.out.println(p1_1);
        p1_1 = qst1_1.getText().toString() + ", " +  p1_1;
        System.out.println(p1_1);
        p1_2 = TextUtils.join(", ", pregunta1_2);
        System.out.println(qst1_2.getText().toString());
        System.out.println(p1_2);
        p1_2 = qst1_2.getText().toString() + ", " +  p1_2;
        p1_3 = TextUtils.join(", ", pregunta1_3);
        System.out.println(qst1_3.getText().toString());
        System.out.println(p1_3);
        p1_3 = qst1_3.getText().toString() + ", " +  p1_3;
        */


        if (checkEmptyData()) {
            Toast.makeText(getApplicationContext(), "Faltan campos por llenar, por favor validelos!",
                    Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(getApplicationContext(), "Bien!",
            //       Toast.LENGTH_LONG).show();
            saveData();
        }
    }

    public boolean checkEmptyData() {
        col = colonia.getText().toString(); //Funcional
        ofi = oficio.getText().toString(); //Funcional
        p3 = pr3.getText().toString();
        p6 = pr6.getText().toString();
        p4_1 = pr4_1.getText().toString();
        p4_2 = pr4_2.getText().toString();
        p4_3 = pr4_3.getText().toString();
        p5_1_1_1 = pr5_1_1.getText().toString();
        p5_1_1_2 = pr5_1_2.getText().toString();
        p5_2_1_1 = pr5_2_1.getText().toString();
        p5_2_1_2 = pr5_2_2.getText().toString();
        p5_3_1_1 = pr5_3_1.getText().toString();
        p5_3_1_2 = pr5_3_2.getText().toString();
        p5_4_1_1 = pr5_4_1.getText().toString();
        p5_4_1_2 = pr5_4_2.getText().toString();
        p5_5_1_1 = pr5_5_1.getText().toString();
        p5_5_1_2 = pr5_5_2.getText().toString();

        boolean isEmpty = true;
        //  && p5 != null && !p5.isEmpty()
        if (col != null && !col.isEmpty() && ofi != null && !ofi.isEmpty() && gen != null && !gen.isEmpty() && eda != null && !eda.isEmpty() && p1_1 != null && !p1_1.isEmpty() && p2 != null && !p2.isEmpty()
                && p3 != null && !p3.isEmpty() && p4_1 != null && !p4_1.isEmpty() && p5_1_1_1 != null && !p5_1_1_1.isEmpty() && p5_1_1_2 != null && !p5_1_1_2.isEmpty() && p5_2_1_1 != null && !p5_2_1_1.isEmpty() && p5_2_1_2 != null && !p5_2_1_2.isEmpty()
                && p5_3_1_1 != null && !p5_3_1_1.isEmpty() && p5_3_1_2 != null && !p5_3_1_2.isEmpty() && p5_4_1_1 != null && !p5_4_1_1.isEmpty()
                && p5_4_1_2 != null && !p5_4_1_2.isEmpty() && p5_5_1_1 != null && !p5_5_1_1.isEmpty() && p5_5_1_2 != null && !p5_5_1_2.isEmpty() && p6 != null && !p6.isEmpty()){
            isEmpty = false;
        }

        return isEmpty;
    }

    private void saveData(){
        String strDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        System.out.println(strDate);
        String chrono = simpleChronometer.getText().toString();
        System.out.println(chrono);
        pDialog.setMessage("Guardando registro...");
        showDialog();
        db.addTopData(userEmail, userSucursal, colony, col, ofi, gen, eda, p1_1, ps1_1_1, ps1_1_2, ps1_1_3, ps1_1_4, p1_2, ps1_2_1, ps1_2_2, ps1_2_3, ps1_2_4, p1_3, ps1_3_1, ps1_3_2, ps1_3_3, ps1_3_4, p2, p3, p4_1, p4_2, p4_3, p5_1_1_1, p5_1_1_2, p5_2_1_1, p5_2_1_2, p5_3_1_1, p5_3_1_2, p5_4_1_1, p5_4_1_2, p5_5_1_1, p5_5_1_2, p6, strDate, chrono);
        Toast.makeText(getApplicationContext(), "Registro exitoso!",
                Toast.LENGTH_LONG).show();
        hideDialog();
        simpleChronometer.stop();
        disableSurvey();
    }

        @Override
    public void onBackPressed() {
        onExitSurvey();
    }

    public void onExitSurvey(){
        new AlertDialog.Builder(this)
                .setTitle("Cerrar encuesta")
                .setMessage("Se cerrara la encuesta sin guardar ¿desea salir?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public void getData() {
        new AlertDialog.Builder(this)
                .setTitle("Guardar encuesta")
                .setMessage("¿Desea guardar los datos almacenados?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkData();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void loadSpinnerDataBranchs() {
        // database handler
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        // Spinner Drop down elements
        List<String> branchs = db.getAllBranchs();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, branchs);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        branchSpinner.setAdapter(dataAdapter);
    }

    private void loadSpinnerDataColonies() {
        // database handler
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        // Spinner Drop down elements
        List<String> colonies = db.getAllColonies(selectedBranch);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, colonies);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        colSpinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // On selecting a spinner item
        selectedBranch = parent.getItemAtPosition(position).toString();
        loadSpinnerDataColonies();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "You selected: " + selectedBranch,
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
