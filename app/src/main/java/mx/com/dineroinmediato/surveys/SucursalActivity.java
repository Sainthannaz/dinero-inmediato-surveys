package mx.com.dineroinmediato.surveys;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import mx.com.dineroinmediato.surveys.tools.SessionManager;

public class SucursalActivity extends AppCompatActivity {
    Button btnSucursal;
    Spinner mySpinner;
    private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnSucursal = (Button) findViewById(R.id.btn_nextDashboar);
        mySpinner = (Spinner) findViewById(R.id.spSucursales);

        session = new SessionManager(getApplicationContext());

        btnSucursal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mySpinner.getSelectedItem().toString();

                if(!text.equals("SUCURSALES")){
                    Toast.makeText(getApplicationContext(),"Has seleccionado " + text , Toast.LENGTH_SHORT).show();
                    session.setSucursal(text);
                    Intent intent = new Intent(SucursalActivity.this,
                            DashboardActivity.class);
                    finish();
                    startActivity(intent);
                }else
                    Snackbar.make(v, "Debes de Seleccionar una sucursal", Snackbar.LENGTH_LONG)
                            .show();


            }
        });

        setSupportActionBar(toolbar);


    }

    @Override
    public void onBackPressed() {

    }

}
