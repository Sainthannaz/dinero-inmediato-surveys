package mx.com.dineroinmediato.surveys;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import com.andreabaccega.widget.FormEditText;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.cketti.library.changelog.ChangeLog;
import mx.com.dineroinmediato.surveys.tools.SessionManager;
import mx.com.dineroinmediato.surveys.tools.VolleySingleton;

public class LoginActivity extends Activity{
    FormEditText inputUser, inputPassword;
    private long back_pressed;
    Button btnLogin;
    private ProgressDialog pDialog;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private SessionManager session;
    String tag_json_obj = "json_obj_req";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        ChangeLog cl = new ChangeLog(this);
        if (cl.isFirstRun()) {
            cl.getLogDialog().show();
        }
        // session manager
        session = new SessionManager(getApplicationContext());
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
            finish();
            startActivity(intent);
        }
        // Edittext
        inputUser = (FormEditText) findViewById(R.id.user);
        inputPassword = (FormEditText) findViewById(R.id.password);
        // Botones de Login y Registro (Este ultimo es temporal)
        btnLogin = (Button) findViewById(R.id.btnLogin);
        // Login button Click Event

        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                FormEditText[] allFields    = { inputUser, inputPassword };
                String user = inputUser.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                boolean allValid = true;
                for (FormEditText field: allFields) {
                    allValid = field.testValidity() && allValid;
                }


                // Si los valores en los campo son validos se procede al logeo en el sistema
                if (allValid) {
                    //sinLogin();
                    checkLogin(user, password);
                    //getBranchs();
                } else {
                    // De lo contrario se muestra un toast indicando que los datos son incorrectos
                    Toast.makeText(getBaseContext(),
                            getString(R.string.error_credentials), Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });


    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.toast_press_again), Toast.LENGTH_SHORT)
                    .show();
        }
        back_pressed = System.currentTimeMillis();
    }


    public void checkLogin(final String email, final String password){
        System.out.println("Tratando de ejecutar solicitud...");
        String tag_string_req = "req_login";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ("http://ventasdi.no-ip.net/WebAPIEncuestas/api/Seguridad/validarCredenciales"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);

                try {
                    System.out.println("Entramos a leer los datos...");
                    JSONObject jObj = new JSONObject(response);
                    //boolean error = jObj.getBoolean("error");
                    //System.out.println("Se evalua si hay error...");
                    String codigo = jObj.getString("codigoRespuesta");
                    String mensaje = jObj.getString("mensajeRespuesta");
                    String nombre = jObj.getString("Nombre");
                    System.out.println("Codigo:" + codigo);
                    if (codigo.equals("000000")){
                        session.createLoginSession(true, "1", "Activo", email);
                        getBranchs();
                        Intent intent = new Intent(LoginActivity.this,
                                DashboardActivity.class);
                        Toast.makeText(getApplicationContext(), "Bienvenido " + nombre, Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(intent);
                    } if (!codigo.equals("000000")){
                        Toast.makeText(getApplicationContext(), "Error: " + mensaje, Toast.LENGTH_LONG).show();
                    }
                    //serverResp.setText(codigo);
                    //serverRespName.setText(mensaje);
                    //JSONObject mensaje = jObj.getJSONObject("mensaje");
                    //System.out.println("mensaje:" + mensaje);
                    //String recibidos = mensaje.getString("mensaje");


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error al obtener datos!" + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("NombreUsuario", email);
                params.put("Contrasenia", password);
                return params;
            }

        };
        VolleySingleton.getInstance(this.getApplicationContext())
                .addToRequestQueue(strReq);
    }

    public void getBranchs(){
        System.out.println("Solicitando sucursales...");
        String tag_string_req = "req_sucursales";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ("http://ventasdi.no-ip.net/WebAPIEncuestas/api/Sucursal/listarSucursales"), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);

                try {
                    System.out.println("Entramos a leer los datos...");
                    JSONObject jObj = new JSONObject(response);
                    String codigo = jObj.getString("codigoRespuesta");
                    String mensaje = jObj.getString("mensajeRespuesta");
                    JSONArray sucursales = jObj.getJSONArray("lista");
                    System.out.println("Codigo:" + codigo);
                    System.out.println("Mensaje:" + mensaje);
                    System.out.println("Sucursales:" + sucursales);
                    if (codigo.equals("000000")) {
                        for (int i = 0; i < sucursales.length(); i++) {
                            JSONObject row = sucursales.getJSONObject(i);
                            String sucursalID = row.getString("sucursalID");
                            String descripcion = row.getString("sucursalID");
                            Toast.makeText(getApplicationContext(), "Sucursales cargadas! ", Toast.LENGTH_LONG).show();
                        }
                    }

                    if (!codigo.equals("000000")){
                        Toast.makeText(getApplicationContext(), "Error: " + mensaje, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error al obtener datos!" + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        VolleySingleton.getInstance(this.getApplicationContext())
                .addToRequestQueue(strReq);
    }



    private void sinLogin(){
        session.setLogin(true);
        Intent intent = new Intent(LoginActivity.this,
                SucursalActivity.class);
        finish();
        startActivity(intent);
    }
}
