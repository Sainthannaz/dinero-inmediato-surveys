package mx.com.dineroinmediato.surveys;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import mx.com.dineroinmediato.surveys.models.Surveys;
import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;

public class ExitSurveyAdapter  extends RecyclerView.Adapter<ExitSurveyAdapter.MyViewHolder> {
    private Context context;
    private Context mContext;
    private List<Surveys> surveysList;
    private ExitSurveyListener listener;
    DatabaseHandler db;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView user, branch, date;
        public ImageButton editButton;


        public MyViewHolder(View view) {
            super(view);
            user = view.findViewById(R.id.user);
            branch = view.findViewById(R.id.branch);
            date = view.findViewById(R.id.date);
            editButton = view.findViewById(R.id.editButton);
            db = new DatabaseHandler(context);
        }
    }

    public ExitSurveyAdapter(Context context, List<Surveys> surveysList) {
        this.context = context;
        this.listener = listener;
        this.surveysList = surveysList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exit_survey_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if(position%2 == 0){
            System.out.println("Gris");
            holder.itemView.setBackgroundColor(0xFFF5F5F5);
        } else {
            System.out.println("Blanco");
            holder.itemView.setBackgroundColor(0xFFFFFFFF);
        }
        final Surveys surveys = surveysList.get(position);
        holder.user.setText(surveys.getsurvey_usuario());
        System.out.println(surveys.getsurvey_usuario());
        holder.branch.setText(surveys.getsurvey_sucursal());
        System.out.println(surveys.getsurvey_sucursal());
        holder.date.setText(surveys.getsurvey_fecha());
        System.out.println(surveys.getsurvey_fecha());
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idSurvey = surveys.getsurvey_id();
                /*
                String usuario = surveys.getsurvey_usuario();
                System.out.println(usuario);
                String sucursal = surveys.getsurvey_sucursal();
                String cliente = surveys.getsurvey_cliente();
                System.out.println(cliente);
                String colonia = surveys.getsurvey_colonia();
                String oficio = surveys.getsurvey_oficio();
                String genero = surveys.getsurvey_genero();
                String edad = surveys.getsurvey_edad();
                String p1 = surveys.getsurvey_p1();
                System.out.println(p1);
                String p2 = surveys.getsurvey_p2();
                String p3 = surveys.getsurvey_p3();
                String p4 = surveys.getsurvey_p4();
                String p5 = surveys.getsurvey_p5();
                String p6 = surveys.getsurvey_p6();
                String p7 = surveys.getsurvey_p7();
                String p8 = surveys.getsurvey_p8();
                String p9 = surveys.getsurvey_p9();
                String p10 = surveys.getsurvey_p10();
                String p11 = surveys.getsurvey_p11();
                String p12 = surveys.getsurvey_p12();
                String p13 = surveys.getsurvey_p13();
                String p14 = surveys.getsurvey_p14();
                String p15 = surveys.getsurvey_p15();
                String p16 = surveys.getsurvey_p16();
                String email = surveys.getsurvey_email();
                String fecha = surveys.getsurvey_fecha();
                String timer = surveys.getsurvey_timer();*/

                System.out.println("ID: " + idSurvey);
                Toast.makeText(context, "Position" + position + ", " + " ID " + idSurvey, Toast.LENGTH_SHORT).show();
                showPopupMenu(holder.editButton, position, idSurvey);
                /*showPopupMenu(holder.editButton, position, idSurvey, usuario, sucursal, cliente, colonia, oficio, genero, edad,
                        p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, email, fecha, timer);*/
            }
        });
    }

    /*private void showPopupMenu(View view, int position, int idSurvey, String usuario, String sucursal, String cliente, String colonia, String oficio, String genero,
                               String edad, String p1, String p2, String p3 , String p4, String p5, String p6, String p7, String p8, String p9, String p10
            , String p11, String p12, String p13, String p14, String p15, String p16, String email, String fecha, String timer) {*/
    private void showPopupMenu(View view, int position, int idSurvey) {
        // inflate menu

        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_edit, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position, idSurvey));
        /*popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position, idSurvey, usuario, sucursal, cliente, colonia, oficio, genero, edad, p1, p2, p3, p4, p5, p6, p7, p8,
                p9, p10, p11, p12, p13, p14, p15, p16, email, fecha, timer));*/
        popup.show();
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        int position;
        int idSurvey;
        String usuario, sucursal, cliente, colonia, oficio, genero, edad, p1, p2, p3 , p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, email, fecha, timer;

        /*public MyMenuItemClickListener(int position, int idSurvey, String usuario, String sucursal, String cliente, String colonia, String oficio, String genero,
                                       String edad, String p1, String p2, String p3 , String p4, String p5, String p6, String p7, String p8, String p9, String p10
                , String p11, String p12, String p13, String p14, String p15, String p16, String email, String fecha, String timer ){}*/
        public MyMenuItemClickListener(int position, int idSurvey ) {

            this.position = position;
            this.idSurvey = idSurvey;
            this.usuario = usuario;
            this.sucursal = sucursal;
            this.cliente = cliente;
            this.colonia = colonia;
            this.oficio = oficio;
            this.genero = genero;
            this.edad = edad;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
            this.p14 = p14;
            this.p15 = p15;
            this.p16 = p16;
            this.email = email;
            this.fecha = fecha;
            this.timer = timer;

        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_view:
                    Toast.makeText(context, "Edit " + idSurvey, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context.getApplicationContext(), ViewExitSurveyActivity.class);
                    //System.out.println("Usuario: " + usuario);
                    //System.out.println("Sucursal: " + sucursal);
                    //System.out.println("Cliente: " + cliente);
                    //System.out.println("P1: " + p1);
                    //
                    //intent.putExtra("usuario", usuario);
                    //intent.putExtra("sucursal", sucursal);
                    //intent.putExtra("cliente", cliente);
                    //intent.putExtra("colonia", colonia);
                    //intent.putExtra("oficio", oficio);
                    //intent.putExtra("genero", genero);
                    //intent.putExtra("edad", edad);
                    //intent.putExtra("p1", p1);
                    /*intent.putExtra("p2", p2);
                    intent.putExtra("p3", p3);
                    intent.putExtra("p4", p4);
                    intent.putExtra("p5", p5);
                    intent.putExtra("p6", p6);
                    intent.putExtra("p7", p7);
                    intent.putExtra("p8", p8);
                    intent.putExtra("p9", p9);
                    intent.putExtra("p10", p10);
                    intent.putExtra("p11", p11);
                    intent.putExtra("p12", p12);
                    intent.putExtra("p13", p13);
                    intent.putExtra("p14", p14);
                    intent.putExtra("p15", p15);
                    intent.putExtra("p16", p16);
                    intent.putExtra("email", email);
                    intent.putExtra("fecha", fecha);
                    intent.putExtra("timer", timer);*/
                    intent.putExtra("idSurvey", idSurvey);
                    context.startActivity(intent);

                    return true;
                case R.id.action_delete:
                    Toast.makeText(context, "Delete" + idSurvey, Toast.LENGTH_SHORT).show();
                    db.DeleteSingleData(idSurvey);
                    surveysList.remove(position);
                    notifyItemRemoved(position);
                    notifyDataSetChanged();
                    return true;
                default:
            }
            return false;
        }
    }


    @Override
    public int getItemCount() {
        return surveysList.size();
    }

    public interface ExitSurveyListener {
        void onSurveysSelected(Surveys surveys);
    }
}
