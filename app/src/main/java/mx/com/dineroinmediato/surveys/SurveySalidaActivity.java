package mx.com.dineroinmediato.surveys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;
import mx.com.dineroinmediato.surveys.tools.SessionManager;


public class SurveySalidaActivity extends Activity {
    Button btnCancelar, btnGuardar;
    View scrollView;
    EditText colonia, oficio, otro, pr16, email, otro_qst2, otro_qst3, otro_qst5, otro_qst7, otro_qst13, otro_qst14;
    RadioGroup sexo, edad, pr4, pr6, pr7, pr8, pr9, pr10, pr11, pr12, pr13, pr14, pr15;
    RadioButton masculino, femenino, edad1, edad2, edad3, edad4, radio_4_1, radio_4_2, radio_6_1, radio_6_2, radio_7_1, radio_7_2, radio_8_1, radio_8_2, radio_8_3, radio_8_4,
    radio_9_1, radio_9_2, radio_9_3, radio_9_4, radio_10_1, radio_10_2, radio_11_1, radio_11_2, radio_12_1, radio_12_2, radio_13_1, radio_13_2, radio_13_3, radio_13_4, radio_14_1, radio_14_2;
    String cli, col, ofi, gen, eda, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, ema;
    CheckBox empeno, pagoparcial, refrendo, pagado, abonorefrendo, chck_1_1, chck_1_2, chck_1_3, chck_1_4, chck_1_5, chck_1_6, chck_2_1, chck_2_2, chck_2_3, chck_2_4, chck_2_5,
            chck_3_1, chck_3_2, chck_3_3, chck_3_4, chck_3_5, chck_3_6, chck_5_1, chck_5_2, chck_5_3, chck_15_1, chck_15_2, chck_15_3, chck_15_4, chck_15_5;
    Chronometer simpleChronometer;
    ArrayList<String> movimiento = new ArrayList<>();
    ArrayList<String> pregunta1 = new ArrayList<>();
    ArrayList<String> pregunta2 = new ArrayList<>();
    ArrayList<String> pregunta3 = new ArrayList<>();
    ArrayList<String> pregunta5 = new ArrayList<>();
    ArrayList<String> pregunta15 = new ArrayList<>();
    FloatingActionButton fab;
    private SessionManager session;
    Date date;
    String userEmail, userSucursal;
    private ProgressDialog pDialog;
    Spinner mySpinner;
    DatabaseHandler db;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        setContentView(R.layout.survey_exit_survey);
        simpleChronometer = (Chronometer) findViewById(R.id.simpleChronometer);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.shake);
        mySpinner = (Spinner) findViewById(R.id.spSucursales);
        loadSpinnerDataBranchs();
        String formatType = simpleChronometer.getFormat();
        btnCancelar = findViewById(R.id.button_cancelar);
        btnGuardar = findViewById(R.id.button_finalizar);
        btnGuardar.setEnabled(false);

        empeno = findViewById(R.id.empeno);
        pagoparcial = findViewById(R.id.pagoparcial);
        refrendo = findViewById(R.id.refrendo);
        pagado = findViewById(R.id.pagado);
        abonorefrendo = findViewById(R.id.abonorefrendo);
        empeno.setEnabled(false);
        pagoparcial.setEnabled(false);
        refrendo.setEnabled(false);
        pagado.setEnabled(false);
        abonorefrendo.setEnabled(false);

        colonia = findViewById(R.id.colonia);
        oficio = findViewById(R.id.oficio);
        colonia.setEnabled(false);
        oficio.setEnabled(false);

        sexo = findViewById(R.id.sexo);
        masculino = findViewById(R.id.masculino);
        femenino = findViewById(R.id.femenino);
        femenino.setEnabled(false);
        masculino.setEnabled(false);

        edad = findViewById(R.id.edad);
        edad1 = findViewById(R.id.edad1);
        edad2 = findViewById(R.id.edad2);
        edad3 = findViewById(R.id.edad3);
        edad4 = findViewById(R.id.edad4);
        edad1.setEnabled(false);
        edad2.setEnabled(false);
        edad3.setEnabled(false);
        edad4.setEnabled(false);

        chck_1_1 = findViewById(R.id.chck_1_1);
        chck_1_2 = findViewById(R.id.chck_1_2);
        chck_1_3 = findViewById(R.id.chck_1_3);
        chck_1_4 = findViewById(R.id.chck_1_4);
        chck_1_5 = findViewById(R.id.chck_1_5);
        chck_1_6 = findViewById(R.id.chck_1_6);
        chck_1_1.setEnabled(false);
        chck_1_2.setEnabled(false);
        chck_1_3.setEnabled(false);
        chck_1_4.setEnabled(false);
        chck_1_5.setEnabled(false);
        chck_1_6.setEnabled(false);

        chck_2_1 = findViewById(R.id.chck_2_1);
        chck_2_2 = findViewById(R.id.chck_2_2);
        chck_2_3 = findViewById(R.id.chck_2_3);
        chck_2_4 = findViewById(R.id.chck_2_4);
        chck_2_5 = findViewById(R.id.chck_2_5);
        otro_qst2 = findViewById(R.id.otro_qst2);
        chck_2_1.setEnabled(false);
        chck_2_2.setEnabled(false);
        chck_2_3.setEnabled(false);
        chck_2_4.setEnabled(false);
        chck_2_5.setEnabled(false);
        otro_qst2.setEnabled(false);

        chck_2_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    otro_qst2.setEnabled(true);
                    otro_qst2.requestFocus();
                } else {
                    otro_qst2.setEnabled(false);
                    otro_qst2.setText("");
                }
            }
        });

        chck_3_1 = findViewById(R.id.chck_3_1);
        chck_3_2 = findViewById(R.id.chck_3_2);
        chck_3_3 = findViewById(R.id.chck_3_3);
        chck_3_4 = findViewById(R.id.chck_3_4);
        chck_3_5 = findViewById(R.id.chck_3_5);
        chck_3_6 = findViewById(R.id.chck_3_6);
        otro_qst3 = findViewById(R.id.otro_qst3);
        chck_3_1.setEnabled(false);
        chck_3_2.setEnabled(false);
        chck_3_3.setEnabled(false);
        chck_3_4.setEnabled(false);
        chck_3_5.setEnabled(false);
        chck_3_6.setEnabled(false);
        otro_qst3.setEnabled(false);

        chck_3_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    otro_qst3.setEnabled(true);
                    otro_qst3.requestFocus();
                } else {
                    otro_qst3.setEnabled(false);
                    otro_qst3.setText("");
                }
            }
        });

        pr4 = findViewById(R.id.qst4);
        radio_4_1 = findViewById(R.id.radio_4_1);
        radio_4_2 = findViewById(R.id.radio_4_2);
        radio_4_1.setEnabled(false);
        radio_4_2.setEnabled(false);

        chck_5_1 = findViewById(R.id.chck_5_1);
        chck_5_2 = findViewById(R.id.chck_5_2);
        chck_5_3 = findViewById(R.id.chck_5_3);
        otro_qst5 = findViewById(R.id.otro_qst5);
        chck_5_1.setEnabled(false);
        chck_5_2.setEnabled(false);
        chck_5_3.setEnabled(false);
        otro_qst5.setEnabled(false);

        chck_5_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    otro_qst5.setEnabled(true);
                    otro_qst5.requestFocus();
                } else {
                    otro_qst5.setEnabled(false);
                    otro_qst5.setText("");
                }
            }
        });

        pr6 = findViewById(R.id.qst6);
        radio_6_1 = findViewById(R.id.radio_6_1);
        radio_6_2 = findViewById(R.id.radio_6_2);
        radio_6_1.setEnabled(false);
        radio_6_2.setEnabled(false);

        pr7 = findViewById(R.id.qst7);
        radio_7_1 = findViewById(R.id.radio_7_1);
        radio_7_2 = findViewById(R.id.radio_7_2);
        otro_qst7 = findViewById(R.id.otro_qst7);
        radio_7_1.setEnabled(false);
        radio_7_2.setEnabled(false);
        otro_qst7.setEnabled(false);

        pr8 = findViewById(R.id.qst8);
        radio_8_1 = findViewById(R.id.radio_8_1);
        radio_8_2 = findViewById(R.id.radio_8_2);
        radio_8_3 = findViewById(R.id.radio_8_3);
        radio_8_4 = findViewById(R.id.radio_8_4);
        radio_8_1.setEnabled(false);
        radio_8_2.setEnabled(false);
        radio_8_3.setEnabled(false);
        radio_8_4.setEnabled(false);

        pr9 = findViewById(R.id.qst9);
        radio_9_1 = findViewById(R.id.radio_9_1);
        radio_9_2 = findViewById(R.id.radio_9_2);
        radio_9_3 = findViewById(R.id.radio_9_3);
        radio_9_4 = findViewById(R.id.radio_9_4);
        radio_9_1.setEnabled(false);
        radio_9_2.setEnabled(false);
        radio_9_3.setEnabled(false);
        radio_9_4.setEnabled(false);

        pr10 = findViewById(R.id.qst10);
        radio_10_1 = findViewById(R.id.radio_10_1);
        radio_10_2 = findViewById(R.id.radio_10_2);
        radio_10_1.setEnabled(false);
        radio_10_2.setEnabled(false);

        pr11 = findViewById(R.id.qst11);
        radio_11_1 = findViewById(R.id.radio_11_1);
        radio_11_2 = findViewById(R.id.radio_11_2);
        radio_11_1.setEnabled(false);
        radio_11_2.setEnabled(false);

        pr12 = findViewById(R.id.qst12);
        radio_12_1 = findViewById(R.id.radio_12_1);
        radio_12_2 = findViewById(R.id.radio_12_2);
        radio_12_1.setEnabled(false);
        radio_12_2.setEnabled(false);

        pr13 = findViewById(R.id.qst13);
        radio_13_1 = findViewById(R.id.radio_13_1);
        radio_13_2 = findViewById(R.id.radio_13_2);
        radio_13_3 = findViewById(R.id.radio_13_3);
        radio_13_4 = findViewById(R.id.radio_13_4);
        otro_qst13 = findViewById(R.id.otro_qst13);
        radio_13_1.setEnabled(false);
        radio_13_2.setEnabled(false);
        radio_13_3.setEnabled(false);
        radio_13_4.setEnabled(false);
        otro_qst13.setEnabled(false);

        pr14 = findViewById(R.id.qst14);
        radio_14_1 = findViewById(R.id.radio_14_1);
        radio_14_2 = findViewById(R.id.radio_14_2);
        otro_qst14 = findViewById(R.id.otro_qst14);
        radio_14_1.setEnabled(false);
        radio_14_2.setEnabled(false);
        otro_qst14.setEnabled(false);

        chck_15_1 = findViewById(R.id.chck_15_1);
        chck_15_2 = findViewById(R.id.chck_15_2);
        chck_15_3 = findViewById(R.id.chck_15_3);
        chck_15_4 = findViewById(R.id.chck_15_4);
        chck_15_5 = findViewById(R.id.chck_15_5);
        chck_15_1.setEnabled(false);
        chck_15_2.setEnabled(false);
        chck_15_3.setEnabled(false);
        chck_15_4.setEnabled(false);
        chck_15_5.setEnabled(false);

        pr16 = findViewById(R.id.qst16);
        pr16.setEnabled(false);

        email = findViewById(R.id.email);
        email.setEnabled(false);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        animation.setRepeatCount(Animation.INFINITE);
        fab.startAnimation(animation);
        date = new Date();
        db = new DatabaseHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> sucursal = session.getUserSucursal();
        userSucursal = sucursal.get(SessionManager.KEY_SUCURSAL);

        // Dialogo de proceso
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        //
        HashMap<String, String> user = session.getUserDetails();
        userEmail = user.get(SessionManager.KEY_EMAIL);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onExitSurvey();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                getData();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = mySpinner.getSelectedItem().toString();

                if(!text.equals("SUCURSALES")){
                    Toast.makeText(getApplicationContext(),"Has seleccionado " + text , Toast.LENGTH_SHORT).show();
                    session.setSucursal(text);
                    HashMap<String, String> sucursal = session.getUserSucursal();
                    userSucursal = sucursal.get(SessionManager.KEY_SUCURSAL);
                    activateSurvey();
                    Toast.makeText(getApplicationContext(), "Inicia la encuesta!",
                            Toast.LENGTH_LONG).show();
                    simpleChronometer.setBase(SystemClock.elapsedRealtime());
                    simpleChronometer.start();
                }else {
                    Snackbar.make(view, "Debes de Seleccionar una sucursal", Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });

        sexo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.masculino:
                        gen = "masculino";
                        break;
                    case R.id.femenino:
                        gen = "femenino";
                        break;
                }
            }
        });

        edad.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.edad1:
                        eda = "18 a 24";
                        break;
                    case R.id.edad2:
                        eda = "25 a 39";
                        break;
                    case R.id.edad3:
                        eda = "40 a 64";
                        break;
                    case R.id.edad4:
                        eda = "65 y mas";
                        break;
                }
            }
        });

        pr4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_4_1:
                        p4 = "Si";
                        break;
                    case R.id.radio_4_2:
                        p4 = "No";
                        break;
                }
            }
        });

        pr6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_6_1:
                        p6 = "Si";
                        break;
                    case R.id.radio_6_2:
                        p6 = "No";
                        break;
                }
            }
        });

        pr7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_7_1:

                        if (otro_qst7.isEnabled()) {
                            otro_qst7.setEnabled(false);
                        } else {
                            otro_qst7.setEnabled(true);
                            otro_qst7.requestFocus();
                        }
                        break;
                    case R.id.radio_7_2:
                        p7 = "No";
                        otro_qst7.setText("");
                        if (otro_qst7.isEnabled()) {
                            otro_qst7.setEnabled(false);
                            otro_qst7.clearFocus();
                        } else {
                            otro_qst7.setEnabled(false);
                            otro_qst7.clearFocus();
                        }
                        break;
                }
            }
        });

        pr8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_8_1:
                        p8 = "De 2 a 5 min";
                        break;
                    case R.id.radio_8_2:
                        p8 = "De 6 a 10 min";
                        break;
                    case R.id.radio_8_3:
                        p8 = "De 10 a 15 min";
                        break;
                    case R.id.radio_8_4:
                        p8 = "Mas de 15 min";
                        break;
                }
            }
        });

        pr9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_9_1:
                        p9 = "Rápido";
                        break;
                    case R.id.radio_9_2:
                        p9 = "Moderadamente rápido";
                        break;
                    case R.id.radio_9_3:
                        p9 = "Lento";
                        break;
                    case R.id.radio_9_4:
                        p9 = "Muy lento";
                        break;
                }
            }
        });

        pr10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_10_1:
                        p10 = "Si";
                        break;
                    case R.id.radio_10_2:
                        p10 = "No";
                        break;
                }
            }
        });

        pr11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_11_1:
                        p11 = "Si";
                        break;
                    case R.id.radio_11_2:
                        p11 = "No";
                        break;
                }
            }
        });

        pr12.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_12_1:
                        p12 = "Si";
                        break;
                    case R.id.radio_12_2:
                        p12 = "No";
                        break;

                }
            }
        });

        pr13.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_13_1:
                        p13 = "Útiles escolares";
                        if (otro_qst13.isEnabled()) {
                            otro_qst13.setEnabled(false);
                        } else {
                            otro_qst13.setEnabled(false);
                        }
                        break;
                    case R.id.radio_13_2:
                        p13 = "Despensa";
                        if (otro_qst13.isEnabled()) {
                            otro_qst13.setEnabled(false);
                        } else {
                            otro_qst13.setEnabled(false);
                        }
                        break;
                    case R.id.radio_13_3:
                        p13 = "Utensilios de cocina";
                        if (otro_qst13.isEnabled()) {
                            otro_qst13.setEnabled(false);
                        } else {
                            otro_qst13.setEnabled(false);
                        }
                        break;
                    case R.id.radio_13_4:
                        if (otro_qst13.isEnabled()) {
                            otro_qst13.setEnabled(false);
                        } else {
                            otro_qst13.setEnabled(true);
                            otro_qst13.requestFocus();

                        }
                        break;
                }
            }
        });

        pr14.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radio_14_1:
                        if (otro_qst14.isEnabled()) {
                            otro_qst14.setEnabled(false);

                        } else {
                            otro_qst14.setEnabled(true);
                            otro_qst14.requestFocus();
                        }
                        break;
                    case R.id.radio_14_2:
                        p14 = "No";
                        otro_qst14.setText("");
                        if (otro_qst14.isEnabled()) {
                            otro_qst14.setEnabled(false);
                            otro_qst14.clearFocus();
                        } else {
                            otro_qst14.setEnabled(false);
                            otro_qst14.clearFocus();
                        }
                        break;

                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        onExitSurvey();
    }

    public void activateSurvey(){

        empeno.setEnabled(true);
        pagoparcial.setEnabled(true);
        refrendo.setEnabled(true);
        pagado.setEnabled(true);
        abonorefrendo.setEnabled(true);
        colonia.setEnabled(true);
        oficio.setEnabled(true);
        femenino.setEnabled(true);
        masculino.setEnabled(true);
        edad1.setEnabled(true);
        edad2.setEnabled(true);
        edad3.setEnabled(true);
        edad4.setEnabled(true);
        chck_1_1.setEnabled(true);
        chck_1_2.setEnabled(true);
        chck_1_3.setEnabled(true);
        chck_1_4.setEnabled(true);
        chck_1_5.setEnabled(true);
        chck_1_6.setEnabled(true);
        chck_2_1.setEnabled(true);
        chck_2_2.setEnabled(true);
        chck_2_3.setEnabled(true);
        chck_2_4.setEnabled(true);
        chck_2_5.setEnabled(true);
        //otro_qst2.setEnabled(true);
        chck_3_1.setEnabled(true);
        chck_3_2.setEnabled(true);
        chck_3_3.setEnabled(true);
        chck_3_4.setEnabled(true);
        chck_3_5.setEnabled(true);
        chck_3_6.setEnabled(true);
        //otro_qst3.setEnabled(true);
        radio_4_1.setEnabled(true);
        radio_4_2.setEnabled(true);
        chck_5_1.setEnabled(true);
        chck_5_2.setEnabled(true);
        chck_5_3.setEnabled(true);
        //otro_qst5.setEnabled(true);
        radio_6_1.setEnabled(true);
        radio_6_2.setEnabled(true);
        radio_7_1.setEnabled(true);
        radio_7_2.setEnabled(true);
        //otro_qst7.setEnabled(true);
        radio_8_1.setEnabled(true);
        radio_8_2.setEnabled(true);
        radio_8_3.setEnabled(true);
        radio_8_4.setEnabled(true);
        radio_9_1.setEnabled(true);
        radio_9_2.setEnabled(true);
        radio_9_3.setEnabled(true);
        radio_9_4.setEnabled(true);
        radio_10_1.setEnabled(true);
        radio_10_2.setEnabled(true);
        radio_11_1.setEnabled(true);
        radio_11_2.setEnabled(true);
        radio_12_1.setEnabled(true);
        radio_12_2.setEnabled(true);
        radio_13_1.setEnabled(true);
        radio_13_2.setEnabled(true);
        radio_13_3.setEnabled(true);
        radio_13_4.setEnabled(true);
        //otro_qst13.setEnabled(true);
        radio_14_1.setEnabled(true);
        radio_14_2.setEnabled(true);
        //otro_qst14.setEnabled(true);
        chck_15_1.setEnabled(true);
        chck_15_2.setEnabled(true);
        chck_15_3.setEnabled(true);
        chck_15_4.setEnabled(true);
        chck_15_5.setEnabled(true);
        pr16.setEnabled(true);
        email.setEnabled(true);
        btnGuardar.setEnabled(true);
        fab.setEnabled(false);
        mySpinner.setEnabled(false);
        sexo.clearCheck();
        edad.clearCheck();
        pr4.clearCheck();
        pr6.clearCheck();
        pr7.clearCheck();
        pr8.clearCheck();
        pr9.clearCheck();
        pr10.clearCheck();
        pr11.clearCheck();
        pr12.clearCheck();
        pr13.clearCheck();
        pr14.clearCheck();
    }
    public void disableSurvey(){

        empeno.setChecked(false);
        pagoparcial.setChecked(false);
        refrendo.setChecked(false);
        pagado.setChecked(false);
        abonorefrendo.setChecked(false);
        empeno.setEnabled(false);
        pagoparcial.setEnabled(false);
        refrendo.setEnabled(false);
        pagado.setEnabled(false);
        abonorefrendo.setEnabled(false);
        colonia.setText("");
        colonia.setEnabled(false);
        oficio.setText("");
        oficio.setEnabled(false);
        femenino.setChecked(false);
        masculino.setChecked(false);
        femenino.setEnabled(false);
        masculino.setEnabled(false);

        edad1.setChecked(false);
        edad2.setChecked(false);
        edad3.setChecked(false);
        edad1.setEnabled(false);
        edad4.setChecked(false);
        edad2.setEnabled(false);
        edad3.setEnabled(false);
        edad4.setEnabled(false);

        chck_1_1.setChecked(false);
        chck_1_2.setChecked(false);
        chck_1_3.setChecked(false);
        chck_1_4.setChecked(false);
        chck_1_5.setChecked(false);
        chck_1_6.setChecked(false);
        chck_1_1.setEnabled(false);
        chck_1_2.setEnabled(false);
        chck_1_3.setEnabled(false);
        chck_1_4.setEnabled(false);
        chck_1_5.setEnabled(false);
        chck_1_6.setEnabled(false);

        chck_2_1.setChecked(false);
        chck_2_2.setChecked(false);
        chck_2_3.setChecked(false);
        chck_2_4.setChecked(false);
        chck_2_5.setChecked(false);
        otro_qst2.setText("");
        chck_2_1.setEnabled(false);
        chck_2_2.setEnabled(false);
        chck_2_3.setEnabled(false);
        chck_2_4.setEnabled(false);
        chck_2_5.setEnabled(false);
        otro_qst2.setEnabled(false);

        chck_3_1.setChecked(false);
        chck_3_2.setChecked(false);
        chck_3_3.setChecked(false);
        chck_3_4.setChecked(false);
        chck_3_5.setChecked(false);
        chck_3_6.setChecked(false);
        otro_qst3.setText("");
        chck_3_1.setEnabled(false);
        chck_3_2.setEnabled(false);
        chck_3_3.setEnabled(false);
        chck_3_4.setEnabled(false);
        chck_3_5.setEnabled(false);
        chck_3_6.setEnabled(false);
        otro_qst3.setEnabled(false);

        radio_4_1.setChecked(false);
        radio_4_2.setChecked(false);
        radio_4_1.setEnabled(false);
        radio_4_2.setEnabled(false);

        chck_5_1.setChecked(false);
        chck_5_2.setChecked(false);
        chck_5_3.setChecked(false);
        otro_qst5.setText("");
        chck_5_1.setEnabled(false);
        chck_5_2.setEnabled(false);
        chck_5_3.setEnabled(false);
        otro_qst5.setEnabled(false);

        radio_6_1.setChecked(false);
        radio_6_2.setChecked(false);
        radio_6_1.setEnabled(false);
        radio_6_2.setEnabled(false);

        radio_7_1.setChecked(false);
        radio_7_2.setChecked(false);
        otro_qst7.setText("");
        radio_7_1.setEnabled(false);
        radio_7_2.setEnabled(false);
        otro_qst7.setEnabled(false);

        radio_8_1.setChecked(false);
        radio_8_2.setChecked(false);
        radio_8_3.setChecked(false);
        radio_8_4.setChecked(false);
        radio_8_1.setEnabled(false);
        radio_8_2.setEnabled(false);
        radio_8_3.setEnabled(false);
        radio_8_4.setEnabled(false);


        radio_9_1.setChecked(false);
        radio_9_2.setChecked(false);
        radio_9_3.setChecked(false);
        radio_9_4.setChecked(false);
        radio_9_1.setEnabled(false);
        radio_9_2.setEnabled(false);
        radio_9_3.setEnabled(false);
        radio_9_4.setEnabled(false);

        radio_10_1.setChecked(false);
        radio_10_2.setChecked(false);
        radio_10_1.setEnabled(false);
        radio_10_2.setEnabled(false);

        radio_11_1.setChecked(false);
        radio_11_2.setChecked(false);
        radio_11_1.setEnabled(false);
        radio_11_2.setEnabled(false);

        radio_12_1.setChecked(false);
        radio_12_2.setChecked(false);
        radio_12_1.setEnabled(false);
        radio_12_2.setEnabled(false);

        radio_13_1.setChecked(false);
        radio_13_2.setChecked(false);
        radio_13_3.setChecked(false);
        radio_13_4.setChecked(false);
        otro_qst13.setText("");
        radio_13_1.setEnabled(false);
        radio_13_2.setEnabled(false);
        radio_13_3.setEnabled(false);
        radio_13_4.setEnabled(false);
        otro_qst13.setEnabled(false);

        radio_14_1.setChecked(false);
        radio_14_2.setChecked(false);
        otro_qst14.setText("");
        radio_14_1.setEnabled(false);
        radio_14_2.setEnabled(false);
        otro_qst14.setEnabled(false);

        chck_15_1.setChecked(false);
        chck_15_2.setChecked(false);
        chck_15_3.setChecked(false);
        chck_15_4.setChecked(false);
        chck_15_5.setChecked(false);
        chck_15_1.setEnabled(false);
        chck_15_2.setEnabled(false);
        chck_15_3.setEnabled(false);
        chck_15_4.setEnabled(false);
        chck_15_5.setEnabled(false);

        pr16.setText("");
        pr16.setEnabled(false);

        email.setText("");
        email.setEnabled(false);

        btnGuardar.setEnabled(false);
        fab.setEnabled(true);
        mySpinner.setEnabled(true);
        sexo.clearCheck();
        edad.clearCheck();
        pr4.clearCheck();
        pr6.clearCheck();
        pr7.clearCheck();
        pr8.clearCheck();
        pr9.clearCheck();
        pr10.clearCheck();
        pr11.clearCheck();
        pr12.clearCheck();
        pr13.clearCheck();
        pr14.clearCheck();
    }

    public void onExitSurvey(){
        new AlertDialog.Builder(this)
                .setTitle("Cerrar encuesta")
                .setMessage("Se cerrara la encuesta sin guardar ¿desea salir?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public void getData() {
        new AlertDialog.Builder(this)
            .setTitle("Guardar encuesta")
            .setMessage("¿Desea guardar los datos almacenados?")
            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkData();

                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            })
            .show();
    }
    private void checkData(){
        System.out.println("Entramos a checar....");
        movimiento.clear();
        pregunta1.clear();
        pregunta2.clear();
        pregunta3.clear();
        pregunta5.clear();
        pregunta15.clear();

        if(empeno.isChecked()){
            movimiento.add("Empeño");
        }
        if(pagoparcial.isChecked()){
            movimiento.add("Pago parcial");
        }
        if(refrendo.isChecked()){
            movimiento.add("Refrendo");
        }
        if(pagado.isChecked()){
            movimiento.add("Desempeño");
        }
        if(abonorefrendo.isChecked()) {
            movimiento.add("Abono y refrendo");
        }

        if(chck_1_1.isChecked()){
            pregunta1.add("Recomendación");
        }
        if(chck_1_2.isChecked()){
            pregunta1.add("Radio");
        }
        if(chck_1_3.isChecked()){
            pregunta1.add("Volante o perifoneo");
        }
        if(chck_1_4.isChecked()){
            pregunta1.add("De paso");
        }
        if(chck_1_5.isChecked()){
            pregunta1.add("Facebook");
        }
        if(chck_1_6.isChecked()){
            pregunta1.add("Eventos");
        }
        /**************************************************/
        if(chck_2_1.isChecked()){
            pregunta2.add("Urgencia médica");
        }
        if(chck_2_2.isChecked()){
            pregunta2.add("Gastos del hogar");
        }
        if(chck_2_3.isChecked()){
            pregunta2.add("Gastos de estudios");
        }
        if(chck_2_4.isChecked()){
            pregunta2.add("Negocio");
        }
        if(chck_2_5.isChecked()){
            pregunta2.add(otro_qst2.getText().toString());
        }
        /**************************************************/
        if(chck_3_1.isChecked()){
            pregunta3.add("Imagen");
        }
        if(chck_3_2.isChecked()){
            pregunta3.add("Seguridad");
        }
        if(chck_3_3.isChecked()){
            pregunta3.add("Rapidez");
        }
        if(chck_3_4.isChecked()){
            pregunta3.add("Monto del préstamo");
        }
        if(chck_3_5.isChecked()){
            pregunta3.add("Tasa de interés");
        }
        if(chck_3_6.isChecked()){
            pregunta3.add(otro_qst3.getText().toString());
        }
        /****************************************************/
        if(chck_5_1.isChecked()){
            pregunta5.add("Una tasa preferencial baja");
        }
        if(chck_5_2.isChecked()){
            pregunta5.add("Un monto de préstamo alto");
        }
        if(chck_5_3.isChecked()){
            pregunta5.add(otro_qst5.getText().toString());
        }
        /**************************************************/
        if(radio_7_1.isChecked()){
            p7 = (otro_qst7.getText().toString());
        }
        /**************************************************/
        if(radio_13_4.isChecked()){
            p13 = (otro_qst13.getText().toString());
        }
        /**************************************************/
        if(radio_14_1.isChecked()){
            p14 = (otro_qst14.getText().toString());
        }
        /**************************************************/
        if(chck_15_1.isChecked()){
            pregunta15.add("Imagen");
        }
        if(chck_15_2.isChecked()){
            pregunta15.add("Seguridad");
        }
        if(chck_15_3.isChecked()){
            pregunta15.add("Rapidez");
        }
        if(chck_15_4.isChecked()){
            pregunta15.add("Monto del préstamo");
        }
        if(chck_15_5.isChecked()){
            pregunta15.add("Tasa de interés");
        }

        cli = TextUtils.join(", ", movimiento);
        System.out.println(cli);
        p1 = TextUtils.join(", ", pregunta1);
        System.out.println(p1);
        p2 = TextUtils.join(", ", pregunta2);
        System.out.println(p2);
        p3 = TextUtils.join(", ", pregunta3);
        System.out.println(p3);
        p5 = TextUtils.join(", ", pregunta5);
        System.out.println(p5);
        p15 = TextUtils.join(", ", pregunta15);
        System.out.println(p15);

        if (checkEmptyData()) {
            Toast.makeText(getApplicationContext(), "Faltan campos por llenar, por favor validelos!",
                    Toast.LENGTH_LONG).show();
        } else {
            //Toast.makeText(getApplicationContext(), "Bien!",
             //       Toast.LENGTH_LONG).show();
            saveData();
        }
    }

    public boolean checkEmptyData(){
        boolean isEmpty = true;
        col = colonia.getText().toString(); //Funcional
        ofi = oficio.getText().toString(); //Funcional
        p16 = pr16.getText().toString(); // Funcional
        ema = email.getText().toString(); // Funcional
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(userEmail);
        System.out.println(userSucursal);
        System.out.println(col);
        System.out.println(ofi);
        System.out.println(gen);
        System.out.println(eda);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        System.out.println(p5);
        System.out.println(p6);
        System.out.println(p7);
        System.out.println(p8);
        System.out.println(p9);
        System.out.println(p10);
        System.out.println(p11);
        System.out.println(p12);
        System.out.println(p13);
        System.out.println(p14);
        System.out.println(p15);
        System.out.println(p16);
        System.out.println(ema);
        if (cli != null && !cli.isEmpty() && col != null && !col.isEmpty() && ofi != null && !ofi.isEmpty() && gen != null && !gen.isEmpty() && eda != null && !eda.isEmpty() && p1 != null && !p1.isEmpty() && p2 != null && !p2.isEmpty()
                && p3 != null && !p3.isEmpty() && p4 != null && !p4.isEmpty() && p5 != null && !p5.isEmpty() && p6 != null && !p6.isEmpty() && p7 != null && !p7.isEmpty() &&
                p8 != null && !p8.isEmpty() && p9 != null && !p9.isEmpty() && p10 != null && !p10.isEmpty() && p11 != null && !p11.isEmpty() && p12 != null && !p12.isEmpty() &&
                p13 != null && !p13.isEmpty() && p14 != null && !p14.isEmpty() && p15 != null && !p15.isEmpty() && p16 != null && !p16.isEmpty()){
            isEmpty = false;
        }
        return isEmpty;
    }

    private void saveData(){

        //DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss zzz");
        //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss zzz");
        String strDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

        //to convert Date to String, use format method of SimpleDateFormat class.
        //String strDate = dateFormat.format(date);
        System.out.println(strDate);
        String chrono = simpleChronometer.getText().toString();
        System.out.println(chrono);
        pDialog.setMessage("Guardando registro...");
        showDialog();
        db.addSurveyData(userEmail, userSucursal, cli, col, ofi, gen, eda, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12,
                p13, p14, p15, p16, ema, strDate, chrono);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println(userEmail);
        System.out.println(userSucursal);
        System.out.println(col);
        System.out.println(ofi);
        System.out.println(gen);
        System.out.println(eda);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        System.out.println(p5);
        System.out.println(p6);
        System.out.println(p7);
        System.out.println(p8);
        System.out.println(p9);
        System.out.println(p10);
        System.out.println(p11);
        System.out.println(p12);
        System.out.println(p13);
        System.out.println(p14);
        System.out.println(p15);
        System.out.println(p16);
        System.out.println(ema);
        System.out.println(strDate);
        hideDialog();
        Toast.makeText(getApplicationContext(), "Registro exitoso!",
                Toast.LENGTH_LONG).show();
        simpleChronometer.stop();
        disableSurvey();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void loadSpinnerDataBranchs() {
        // database handler
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        // Spinner Drop down elements
        List<String> branchs = db.getAllBranchs();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, branchs);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mySpinner.setAdapter(dataAdapter);
    }
}
