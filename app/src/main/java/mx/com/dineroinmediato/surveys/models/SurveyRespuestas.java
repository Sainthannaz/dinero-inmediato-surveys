package mx.com.dineroinmediato.surveys.models;

public class SurveyRespuestas {
    int respuesta_id, pregunta_id, survey_id;
    String respuesta, respuesta_opinion;

    public SurveyRespuestas() {

    }

    public SurveyRespuestas(int respuesta_id, int pregunta_id, int survey_id, String respuesta, String respuesta_opinion){
        this.respuesta_id = respuesta_id;
        this.pregunta_id = pregunta_id;
        this.survey_id = survey_id;
        this.respuesta = respuesta;
        this.respuesta_opinion = respuesta_opinion;
    }

    public int getrespuesta_id() {
        return respuesta_id;
    }
    public void setrespuesta_id(int id) {
        this.respuesta_id = id;
    }

    public int getpregunta_id() {
        return pregunta_id;
    }
    public void setpregunta_id(int id) {
        this.pregunta_id = id;
    }

    public int getsurvey_id() {
        return survey_id;
    }
    public void setsurvey_id(int id) {
        this.survey_id = id;
    }

    public String getrespuesta() {
        return respuesta;
    }
    public void setrespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getrespuesta_opinion() {
        return respuesta_opinion;
    }
    public void setrespuesta_opinion(String respuesta_opinion) {
        this.respuesta_opinion = respuesta_opinion;
    }
}
