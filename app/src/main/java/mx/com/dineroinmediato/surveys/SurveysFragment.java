package mx.com.dineroinmediato.surveys;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import mx.com.dineroinmediato.surveys.tools.PermissionUtils;

public class SurveysFragment extends Fragment {
    private ProgressDialog pDialog;
    Button launchSurvey, launchSurveyTopOfMind;
    private static final int MY_READ_WRITE_PERMISSION_REQUEST_CODE = 1;
    private static final int READ_WRITE_PERMISSION_REQUEST_CODE = 2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.surveys_fragment,null);
        requestReadWritePermission(MY_READ_WRITE_PERMISSION_REQUEST_CODE);
        launchSurvey = view.findViewById(R.id.button_static);
        launchSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSurvey(view);

            }
        });

        launchSurveyTopOfMind = view.findViewById(R.id.button_topmind);
        launchSurveyTopOfMind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSurveyTopOfMind(view);

            }
        });

        return view;

    }

    private void launchSurvey(View view){
        Intent intent = new Intent(getActivity(), SurveySalidaActivity.class);
        startActivity(intent);
    }

    private void launchSurveyTopOfMind(View view){
        Intent intent = new Intent(getActivity(), SurveyTopMindActivity.class);
        startActivity(intent);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void requestReadWritePermission(int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog
                    .newInstance(requestCode, false).show(
                    getFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            PermissionUtils.requestPermission((AppCompatActivity) getContext(), requestCode,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == READ_WRITE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {

            }
        } else if (requestCode == READ_WRITE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location layer if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

            } else {
                //
            }
        }
    }
}
