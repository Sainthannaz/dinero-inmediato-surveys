package mx.com.dineroinmediato.surveys.models;

public class SurveyTop {
    int survey_id;
    String survey_usuario, survey_sucursal, survey_cp, survey_colonia, survey_oficio, survey_genero, survey_edad, survey_fecha, survey_timer;

    public SurveyTop(){

    }

    public SurveyTop(int survey_id, String survey_usuario, String survey_sucursal, String survey_cp, String survey_colonia,
                     String survey_oficio, String survey_genero, String survey_edad, String survey_fecha, String survey_timer ){

        this.survey_id = survey_id;
        this.survey_usuario = survey_usuario;
        this.survey_sucursal = survey_sucursal;
        this.survey_cp = survey_cp;
        this.survey_colonia = survey_colonia;
        this.survey_oficio = survey_oficio;
        this.survey_genero = survey_genero;
        this.survey_edad = survey_edad;
        this.survey_fecha = survey_fecha;
        this.survey_timer = survey_timer;
    }

    public int getsurvey_id() {
        return survey_id;
    }
    public void setsurvey_id(int id) {
        this.survey_id = id;
    }

    public String getsurvey_usuario() {
        return survey_usuario;
    }
    public void setsurvey_usuario(String usuario) {
        this.survey_usuario = usuario;
    }

    public String getsurvey_sucursal() {
        return survey_sucursal;
    }
    public void setsurvey_sucursal(String sucursal) {
        this.survey_sucursal = sucursal;
    }

    public String getsurvey_cp() {
        return survey_cp;
    }
    public void setsurvey_cp(String zona) {
        this.survey_cp = zona;
    }

    public String getsurvey_colonia() {
        return survey_colonia;
    }
    public void setsurvey_colonia(String colonia) {
        this.survey_colonia = colonia;
    }

    public String getsurvey_oficio() {
        return survey_oficio;
    }
    public void setsurvey_oficio(String oficio) {
        this.survey_oficio = oficio;
    }

    public String getsurvey_genero() {
        return survey_genero;
    }
    public void setsurvey_genero(String genero) {
        this.survey_genero = genero;
    }

    public String getsurvey_edad() {
        return survey_edad;
    }
    public void setsurvey_edad(String edad) {
        this.survey_edad = edad;
    }

    public String getsurvey_fecha() {
        return survey_fecha;
    }
    public void setsurvey_fecha(String fecha) {
        this.survey_fecha = fecha;
    }

    public String getsurvey_timer() {
        return survey_timer;
    }
    public void setsurvey_timer(String timer) {
        this.survey_timer = timer;
    }

}
