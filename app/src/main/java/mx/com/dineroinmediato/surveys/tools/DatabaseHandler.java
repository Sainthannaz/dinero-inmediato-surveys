package mx.com.dineroinmediato.surveys.tools;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import mx.com.dineroinmediato.surveys.models.SurveyDetalle;
import mx.com.dineroinmediato.surveys.models.SurveyRespuestas;
import mx.com.dineroinmediato.surveys.models.SurveyTop;
import mx.com.dineroinmediato.surveys.models.Surveys;

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "surveys";

    // Contacts table name
    public static final String TABLE_ENCUESTAS = "encuestas";
    public static final String TABLE_ECTCATENCUESTA = "ECTCATENCUESTA";
    public static final String TABLE_ECTDIVISION = "ECTDIVISION";
    public static final String TABLE_ECTPREGUNTA = "ECTPREGUNTA";
    public static final String TABLE_ECTENCUESTA = "ECTENCUESTA";
    public static final String TABLE_ECTRESPUESTA = "ECTRESPUESTA";
    public static final String TABLE_ECTDETALLE = "ECTDETALLE";
    public static final String TABLE_ECTCATRESPUESTA = "ECTCATRESPUESTA";
    public static final String TABLE_ECTCATTIPOPREGUNTA = "ECTCATTIPOPREGUNTA";
    public static final String TABLE_ECTCATTIPORESPUESTA = "ECTCATTIPORESPUESTA";
    public static final String TABLE_ECTSUCURSAL = "sucursal";
    public static final String TABLE_ECTCOLONIA = "colonia";

    public static final String TABLE_TOP = "top";

    // Surveys data

    public static final String ID = "id";
    public static final String SURVEY_USUARIO = "Usuario";
    public static final String SURVEY_SUCURSAL = "Sucursal";
    public static final String SURVEY_ZONA = "Zona";
    public static final String SURVEY_CLIENTE = "Cliente";
    public static final String SURVEY_COLONIA = "Colonia";
    public static final String SURVEY_CP = "CP";
    public static final String SURVEY_OFICIO = "Oficio";
    public static final String SURVEY_GENERO = "Genero";
    public static final String SURVEY_EDAD = "Edad";
    public static final String SURVEY_P1 = "P1";
    public static final String SURVEY_P2 = "P2";
    public static final String SURVEY_P3 = "P3";
    public static final String SURVEY_P4 = "P4";
    public static final String SURVEY_P5 = "P5";
    public static final String SURVEY_P6 = "P6";
    public static final String SURVEY_P7 = "P7";
    public static final String SURVEY_P8 = "P8";
    public static final String SURVEY_P9 = "P9";
    public static final String SURVEY_P10 = "P10";
    public static final String SURVEY_P11 = "P11";
    public static final String SURVEY_P12 = "P12";
    public static final String SURVEY_P13 = "P13";
    public static final String SURVEY_P14 = "P14";
    public static final String SURVEY_P15 = "P15";
    public static final String SURVEY_P16 = "P16";
    public static final String SURVEY_EMAIL = "Email";
    public static final String SURVEY_FECHA = "Fecha";
    public static final String SURVEY_TIMER = "Tiempo";

    public static final String EncuestaID = "EncuestaId";
    public static final String DetalleID = "DetalleId";
    public static final String Nombre = "Nombre";
    public static final String FechaAlta = "FechaAlta";
    public static final String Estatus = "Estatus";
    public static final String UsuarioID = "UsuarioId";
    public static final String FechaActual = "FechaActual";
    public static final String SucursalID = "SucursalID";
    public static final String NumeroTransac = "NumeroTransac";
    public static final String DivisionID = "DivisionID";
    public static final String DivisionPadre = "DivisionPadre";
    public static final String Descripcion = "Descripcion";
    public static final String PreguntaID = "PreguntaID";
    public static final String Pregunta = "Pregunta";
    public static final String Obligatorio = "Obligatorio";
    public static final String TipoPreguntaID = "TipoPreguntaID";
    public static final String CatEncuestaID = "CatEncuestaID";
    public static final String FechaAplicacion = "FechaAplicacion";
    public static final String RespuestaID = "RespuestaID";
    public static final String Respuesta = "Respuesta";
    public static final String RespOpinion = "RespOpinion";
    public static final String TipoRespuestaID = "TipoRespuestaID";
    public static final String ValorRespuesta = "ValorRespuesta";
    public static final String CP = "CP";
    public static final String ASENTAMIENTO = "Asentamiento";
    public static final String TIPO = "Tipo";
    public static final String SUCURSAL = "Sucursal";
    public static final String MUNICIPIO = "Municipio";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_META_TABLE = "CREATE TABLE " + TABLE_ENCUESTAS + "("
                + ID + " INTEGER PRIMARY KEY," + SURVEY_USUARIO + " TEXT," + SURVEY_SUCURSAL + " TEXT,"
                + SURVEY_CLIENTE + " TEXT," + SURVEY_COLONIA + " TEXT," + SURVEY_OFICIO + " TEXT," + SURVEY_GENERO + " TEXT," + SURVEY_EDAD + " TEXT," +   SURVEY_P1 + " TEXT," +
                SURVEY_P2 + " TEXT," +  SURVEY_P3 + " TEXT," +  SURVEY_P4 + " TEXT," +  SURVEY_P5 + " TEXT," +  SURVEY_P6 + " TEXT," +
                SURVEY_P7 + " TEXT," +  SURVEY_P8 + " TEXT," +  SURVEY_P9 + " TEXT," +  SURVEY_P10 + " TEXT," +  SURVEY_P11 + " TEXT," +
                SURVEY_P12 + " TEXT," +  SURVEY_P13 + " TEXT," +  SURVEY_P14 + " TEXT," +  SURVEY_P15 + " TEXT," +  SURVEY_P16 + " TEXT," + SURVEY_EMAIL + " TEXT," + SURVEY_FECHA + " TEXT," + SURVEY_TIMER + " TEXT" + ")";

        String CREATE_TOP_TABLE = "CREATE TABLE " + TABLE_TOP + "("
                + ID + " INTEGER PRIMARY KEY," + SURVEY_USUARIO + " TEXT," + SURVEY_SUCURSAL + " TEXT,"
                + SURVEY_CP + " TEXT," + SURVEY_COLONIA + " TEXT," + SURVEY_OFICIO + " TEXT," + SURVEY_GENERO + " TEXT," + SURVEY_EDAD + " TEXT," +   SURVEY_P1 + " TEXT," +
                SURVEY_P2 + " TEXT," +  SURVEY_P3 + " TEXT," +  SURVEY_P4 + " TEXT," +  SURVEY_P5 + " TEXT," +  SURVEY_P6 + " TEXT," +  SURVEY_FECHA + " TEXT," + SURVEY_TIMER + " TEXT" + ")";

        /* ERROR EN COLUMNA TEXT
        String CREATE_TOP_TABLE = "CREATE TABLE " + TABLE_TOP + "("
                + ID + " INTEGER PRIMARY KEY," + SURVEY_USUARIO + " TEXT," + SURVEY_SUCURSAL + " TEXT,"
                + SURVEY_CP + " TEXT," + SURVEY_COLONIA + " TEXT," + SURVEY_OFICIO + " TEXT," + SURVEY_GENERO + " TEXT," + SURVEY_EDAD + " TEXT," +   SURVEY_P1 + " TEXT," +
                SURVEY_P2 + " TEXT," +  SURVEY_P3 + " TEXT," +  SURVEY_P4 + " TEXT," +  SURVEY_P5 + " TEXT," +  SURVEY_P6 + " TEXT," +  " TEXT," + SURVEY_FECHA + " TEXT," + SURVEY_TIMER + " TEXT" + ")";
        */

        /*
        String CREATE_TOP_TABLE = "CREATE TABLE " + TABLE_TOP + "("
                + ID + " INTEGER PRIMARY KEY," + SURVEY_USUARIO + " TEXT," + SURVEY_SUCURSAL + " TEXT,"
                + SURVEY_CLIENTE + " TEXT," + SURVEY_COLONIA + " TEXT," + SURVEY_OFICIO + " TEXT," + SURVEY_GENERO + " TEXT," + SURVEY_EDAD + " TEXT," +   SURVEY_P1 + " TEXT," +
                SURVEY_P2 + " TEXT," +  SURVEY_P3 + " TEXT," +  SURVEY_P4 + " TEXT," +  SURVEY_P5 + " TEXT," +  SURVEY_P6 + " TEXT," +  " TEXT," + SURVEY_FECHA + " TEXT" + ")";
        */

        String CREATE_ECTCATENCUESTA_TABLE = "CREATE TABLE " + TABLE_ECTCATENCUESTA + "("
                + EncuestaID + " INTEGER PRIMARY KEY," + Nombre + " TEXT," + FechaAlta + " TEXT,"
                + Estatus + " TEXT," + UsuarioID + " INTEGER," + FechaActual + " TEXT," + SucursalID + " INTEGER," + NumeroTransac + " INT" + ")";

        String CREATE_ECTDIVISION_TABLE = "CREATE TABLE " + TABLE_ECTDIVISION + "("
                + DivisionID + " INTEGER PRIMARY KEY," + EncuestaID + " INTEGER," + DivisionPadre + " INTEGER,"
                + Estatus + " TEXT," + Descripcion + " TEXT," + UsuarioID + " INTEGER," + FechaActual + " TEXT," + SucursalID + " INTEGER," + NumeroTransac + "INTEGER" + ")";

        String CREATE_ECTPREGUNTA_TABLE = "CREATE TABLE " + TABLE_ECTPREGUNTA + "("
                + PreguntaID + " INTEGER PRIMARY KEY," + Pregunta + " TEXT," + DivisionID + " INTEGER,"
                + EncuestaID + " INTEGER," + Obligatorio + " TEXT," + TipoPreguntaID + " INTEGER," + UsuarioID + " INTEGER," + FechaActual + " TEXT," + SucursalID + "INTEGER" + ")";

        String CREATE_ECTENCUESTA_TABLE = "CREATE TABLE " + TABLE_ECTENCUESTA + "("
                + EncuestaID + " INTEGER PRIMARY KEY," + CatEncuestaID + " INTEGER," + FechaAplicacion + " TEXT,"
                + SucursalID + " INTEGER," + UsuarioID + " INTEGER" + ")";

        String CREATE_ECTRESPUESTA_TABLE = "CREATE TABLE " + TABLE_ECTRESPUESTA + "("
                + RespuestaID + " INTEGER PRIMARY KEY," + PreguntaID + " INTEGER," + EncuestaID + " INTEGER,"
                + Respuesta + " TEXT," + RespOpinion + " TEXT" + ")";

        String CREATE_ECTDETALLE_TABLE = "CREATE TABLE " + TABLE_ECTDETALLE + "("
                + DetalleID + " INTEGER PRIMARY KEY," + RespuestaID + " INTEGER," + PreguntaID + " INTEGER," + EncuestaID + " INTEGER,"
                + Respuesta + " TEXT" + ")";

        String CREATE_ECTSUCURSAL_TABLE = "CREATE TABLE " + TABLE_ECTSUCURSAL + "("
                + ID + " INTEGER PRIMARY KEY," + SUCURSAL + " TEXT," + CP + " TEXT" + ")";

        String CREATE_ECTCOLONIA_TABLE = "CREATE TABLE " + TABLE_ECTCOLONIA + "("
                + ID + " INTEGER PRIMARY KEY," + CP + " TEXT," + ASENTAMIENTO + " TEXT,"
                + SUCURSAL + " TEXT," + MUNICIPIO + " TEXT" + ")";


        db.execSQL(CREATE_META_TABLE);
        System.out.println("META CREADA");

        db.execSQL(CREATE_TOP_TABLE);
        db.execSQL(CREATE_ECTCATENCUESTA_TABLE);

        db.execSQL(CREATE_ECTDIVISION_TABLE);
        db.execSQL(CREATE_ECTPREGUNTA_TABLE);
        db.execSQL(CREATE_ECTDETALLE_TABLE);
        db.execSQL(CREATE_ECTENCUESTA_TABLE);
        db.execSQL(CREATE_ECTRESPUESTA_TABLE);

        db.execSQL(CREATE_ECTCOLONIA_TABLE);
        db.execSQL(CREATE_ECTSUCURSAL_TABLE);
        System.out.println("SUCURSAL CREADA");

        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('SUCURSALES', '00000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('CANTARITOS', '97203')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('CAUCEL', '97314')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('CENTRO 67', '97000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('CENTRO 69', '97000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('CHUBURNÁ', '97205')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('JUAN PABLO II', '97246')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('KANASÍN', '97370')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('LOS COCOS', '97000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('MARÍA LUISA', '97199')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('MEJORADA', '97000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('ORIENTE', '97168')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('PREPA 1', '97150')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('PROGRESO', '97320')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('SANTIAGO', '97000')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('TIZIMÍN', '97700')");
        db.execSQL("INSERT INTO " + TABLE_ECTSUCURSAL + "(Sucursal, CP) VALUES ('UMÁN', '97390')");


        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('00000', 'SUCURSALES', 'SUCURSALES', 'SUCURSALES')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Jardines de San Sebastian', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'La Quinta', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Los Cocos', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Mérida Centro', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Privada Del Maestro', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Privada Garcia Gineres C - 29', 'LOS COCOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97150', 'Industrial', 'PREPA 1', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97150', 'Trava Quintero', 'PREPA 1', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Almendros', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Ampliación Francisco de Montejo', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Arboledas Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Arcos del Sol', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Arekas', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Aurea Residencial', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Brisas de Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Camara de La Construcción', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Chuburna de Hidalgo III', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Chuburna de Hidalgo IV', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Chuburna de Hidalgo V', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Cocoteros', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Cordeleros de Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Del Bosque', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'El Conquistador', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'El Prado', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Francisco de Montejo', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Francisco de Montejo II', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Francisco de Montejo III', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Francisco de Montejo IV', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Francisco de Montejo V', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Industrias No Contaminantes', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'La Castellana', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Las Haciendas III', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Lotificacion Chuburna de Hidalgo I', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Lotificacion Chuburna de Hidalgo II', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Magnolias', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Mérida', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Nueva San Jose Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Nuevo San José', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Platino', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Privada Chuburna de Hidalgo (II)', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Privada Chuburna Plus', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Privada las Palmas', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Privada Palma Corozal', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Privada Villa Palma Real', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Puesta del Sol', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Rincón Colonial', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'San Francisco Chuburna II', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'San Pablo', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'San Pedro Uxmal', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Terranova', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Tulias de Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Villas Del Prado', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Villas las Palmas', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97203', 'Vista Alegre de Chuburna', 'CANTARITOS', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Arboleda', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Balcones II', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Boulevares', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Centenario Cámara de Comercio Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Cerradas de la Herradura', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Ciricotes de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Ciudad Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Ciudad Caucel II', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Gran Herradura', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Gran Santa Fe', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Hacienda Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Hogares Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Horizontes de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Jardines de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Jardines de Poniente', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Jardines del Caucel II', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Ceiba', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Ciudadela', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Herradura', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Herradura II', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Herradura III', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Herradura IV', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'La Perla Ciudad Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Las Torres', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Las Torres I', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Las Torres II', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Las Torres III', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Los Almendros', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Los Balcones', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Los Cocos de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Paseos de Opichen la Joya', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Pedregales de Ciudad Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Rinconada de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Sasula', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Sol Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Sol Caucel III', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Susulá', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Terranova Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Villa Jardín', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Villas de Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97314', 'Viva Caucel', 'CAUCEL', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Boulevares de Chuburna', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Bugambilias', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Chuburna de Hidalgo', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Chuburna Inn', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'El Campanario', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'El Cortijo I', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'El Cortijo II', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Floresta', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Juan B Sosa', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Las Hadas', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Loma Bonita', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Loma Bonita Xcumpich', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Paseo de Montejo', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Pinzon', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Arboledas', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Campestre Chuburna', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Chuburna de Hidalgo I', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Cortijo', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada las Flores', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Pedregal II', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Privada Turquesa', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Residencial Hacienda Real', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'San Ángel', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'San Luis Chuburna', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Tabachines', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97205', 'Villa Carmencita', 'CHUBURNÁ', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Ampliación Juan Pablo II', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Anexo Juan Pablo II', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Angeles II', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Bosques de Mulsay', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Bosques de Yucalpeten', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Bosques Del Poniente', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Granja Fruticola Susula', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Hacienda Mulsay', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Jardines de Nueva Mulsay', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Juan Pablo II', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Juan Pablo II Cardenales', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Juan Pablo II Secc. Mérida 2000', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'La Reja', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Los Ángeles', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'México Poniente', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Mulsay', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Mulsay de La Magdalena', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Mulsay Polígono', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Mulsay Solidaridad', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Paseos de Opichen', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'San Jose Xoclan', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Villas de Tixcacal', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Xoclan', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Xoclan Carmelitas', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Xoclan Rejas', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97246', 'Xoclan Xbech', 'JUAN PABLO II', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97199', 'Maria Luisa', 'MARÍA LUISA', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97168', 'Cerillera', 'ORIENTE', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97168', 'Cortes Sarmiento', 'ORIENTE', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97168', 'Del Carmen', 'ORIENTE', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97168', 'Jardines de Miraflores', 'ORIENTE', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97168', 'Madrid', 'ORIENTE', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97700', 'Tizimín', 'TIZIMÍN', 'TIZIMÍN')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97390', 'Umán', 'UMÁN', 'UMÁN')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Centro 67', 'CENTRO 67', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Centro 69', 'CENTRO 69', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97370', 'Kasín', 'KASÍN', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Mejorada', 'MEJORADA', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97000', 'Santiago', 'SANTIAGO', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97320', '23 de Noviembre','PROGRESO', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97320', 'Ciénega 2000','PROGRESO', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97320', 'Costa Azul', 'PROGRESO', 'MÉRIDA')");
        db.execSQL("INSERT INTO " + TABLE_ECTCOLONIA + "(CP, Asentamiento, Sucursal, Municipio ) VALUES ('97320', 'Feliciano Canul Reyes', 'PROGRESO', 'MÉRIDA')");        System.out.println("Base de datos creada...META");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENCUESTAS);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTCATENCUESTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTDIVISION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTPREGUNTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTENCUESTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTRESPUESTA);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTCOLONIA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ECTSUCURSAL);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    public void addSurveyData(String usuario, String sucursal, String cliente, String colonia, String oficio, String genero, String edad, String p1, String p2,  String p3,
                              String p4, String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13,
                              String p14, String p15, String p16, String email, String fecha, String timer) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        //values.put(KEY_ID, customer_id);
        values.put(SURVEY_USUARIO, usuario);
        values.put(SURVEY_SUCURSAL, sucursal);
        values.put(SURVEY_CLIENTE, cliente);
        values.put(SURVEY_COLONIA, colonia);
        values.put(SURVEY_OFICIO, oficio);
        values.put(SURVEY_GENERO, genero);
        values.put(SURVEY_EDAD, edad);
        values.put(SURVEY_P1, p1);
        values.put(SURVEY_P2, p2);
        values.put(SURVEY_P3, p3);
        values.put(SURVEY_P4, p4);
        values.put(SURVEY_P5, p5);
        values.put(SURVEY_P6, p6);
        values.put(SURVEY_P7, p7);
        values.put(SURVEY_P8, p8);
        values.put(SURVEY_P9, p9);
        values.put(SURVEY_P10, p10);
        values.put(SURVEY_P11, p11);
        values.put(SURVEY_P12, p12);
        values.put(SURVEY_P13, p13);
        values.put(SURVEY_P14, p14);
        values.put(SURVEY_P15, p15);
        values.put(SURVEY_P16, p16);
        values.put(SURVEY_EMAIL, email);
        values.put(SURVEY_FECHA, fecha);
        values.put(SURVEY_TIMER, timer);
        db.insert(TABLE_ENCUESTAS, null, values);
        db.close(); // Closing database connection
    }

    public void addTopData(String usuario, String sucursal, String cp, String colonia, String oficio, String genero, String edad, String p1_1, String ps1_1_1, String ps1_1_2, String ps1_1_3, String ps1_1_4,
                           String p1_2, String ps1_2_1, String ps1_2_2, String ps1_2_3, String ps1_2_4, String p1_3, String ps1_3_1, String ps1_3_2, String ps1_3_3, String ps1_3_4,
                           String p2, String p3, String p4_1, String p4_2, String p4_3, String p5_1_1_1, String p5_1_1_2, String p5_2_1_1, String p5_2_1_2, String p5_3_1_1, String p5_3_1_2, String p5_4_1_1, String p5_4_1_2, String p5_5_1_1, String p5_5_1_2, String p6, String fecha, String timer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(KEY_ID, customer_id);
        values.put(SURVEY_USUARIO, usuario);
        values.put(SURVEY_SUCURSAL, sucursal);
        values.put(SURVEY_CP, cp);
        values.put(SURVEY_COLONIA, colonia);
        values.put(SURVEY_OFICIO, oficio);
        values.put(SURVEY_GENERO, genero);
        values.put(SURVEY_EDAD, edad);
        values.put(SURVEY_FECHA, fecha);
        values.put(SURVEY_TIMER, timer);
        long survey_id = db.insert(TABLE_TOP, null, values);
        createResponse(survey_id, p1_1, ps1_1_1, ps1_1_2, ps1_1_3, ps1_1_4, p1_2, ps1_2_1, ps1_2_2, ps1_2_3, ps1_2_4, p1_3, ps1_3_1, ps1_3_2, ps1_3_3, ps1_3_4, p2, p3, p4_1, p4_2, p4_3, p5_1_1_1, p5_1_1_2, p5_2_1_1, p5_2_1_2, p5_3_1_1, p5_3_1_2, p5_4_1_1, p5_4_1_2, p5_5_1_1, p5_5_1_2, p6);
        db.close();
    }

    public void createResponse(long survey_id, String p1_1, String ps1_1_1, String ps1_1_2, String ps1_1_3, String ps1_1_4,
                               String p1_2, String ps1_2_1, String ps1_2_2, String ps1_2_3, String ps1_2_4,
                               String p1_3, String ps1_3_1, String ps1_3_2, String ps1_3_3, String ps1_3_4, String p2, String p3,
                               String p4_1, String p4_2, String p4_3, String p5_1_1_1, String p5_1_1_2, String p5_2_1_1, String p5_2_1_2,
                               String p5_3_1_1, String p5_3_1_2, String p5_4_1_1, String p5_4_1_2, String p5_5_1_1, String p5_5_1_2, String p6){

        System.out.println(survey_id);
        System.out.println(p1_1);
        System.out.println(p1_2);
        System.out.println(p1_3);

        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4_1);
        System.out.println(p4_2);
        System.out.println(p4_3);
        System.out.println(p5_1_1_1);
        System.out.println(p5_1_1_2);
        System.out.println(p5_2_1_1);
        System.out.println(p5_2_1_2);
        System.out.println(p5_3_1_1);
        System.out.println(p5_3_1_2);
        System.out.println(p5_4_1_1);
        System.out.println(p5_4_1_2);
        System.out.println(p5_5_1_1);
        System.out.println(p5_5_1_2);
        System.out.println(p6);;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 1);
        values.put(Respuesta, p1_1);
       // db.insert(TABLE_ECTRESPUESTA, null, values);
        long respuesta_id_1 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_1(respuesta_id_1, survey_id, ps1_1_1, ps1_1_2, ps1_1_3, ps1_1_4);

        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 1);
        values.put(Respuesta, p1_2);
        //db.insert(TABLE_ECTRESPUESTA, null, values);
        long respuesta_id_2 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_2(respuesta_id_2, survey_id, ps1_2_1, ps1_2_2, ps1_2_3, ps1_2_4);

        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 1);
        values.put(Respuesta, p1_3);
        //db.insert(TABLE_ECTRESPUESTA, null, values);
        long respuesta_id_3 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_3(respuesta_id_3, survey_id, ps1_3_1, ps1_3_2, ps1_3_3, ps1_3_4);

        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 2);
        values.put(Respuesta, p2);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);
        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 3);
        values.put(Respuesta, p3);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);
        values.put(EncuestaID, survey_id);
        values.put(PreguntaID, 4);
        values.put(Respuesta, p4_1);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);
        values.put(PreguntaID, 4);
        values.put(Respuesta, p4_2);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);
        values.put(PreguntaID, 4);
        values.put(Respuesta, p4_3);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);

        values.put(PreguntaID, 5);
        values.put(Respuesta, p5_1_1_1);     ;
        long respuesta_id_51 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_5_1(respuesta_id_51, survey_id, p5_1_1_2 );

        //values.put(Respuesta, p5_1_1_2);     ;
         //db.insert(TABLE_ECTRESPUESTA, null, values);


        values.put(PreguntaID, 5);
        values.put(Respuesta, p5_2_1_1);     ;
        long respuesta_id_52 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_5_2(respuesta_id_52, survey_id, p5_2_1_2 );

        values.put(PreguntaID, 5);
        values.put(Respuesta, p5_3_1_1);     ;
        long respuesta_id_53 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_5_3(respuesta_id_53, survey_id, p5_3_1_2 );

        //values.put(Respuesta, p5_3_1_1);     ;
        //db.insert(TABLE_ECTRESPUESTA, null, values);
        //values.put(Respuesta, p5_3_1_2);     ;
        //db.insert(TABLE_ECTRESPUESTA, null, values);

        values.put(PreguntaID, 5);
        values.put(Respuesta, p5_4_1_1);     ;
        long respuesta_id_54 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_5_4(respuesta_id_54, survey_id, p5_4_1_2 );

        //values.put(PreguntaID, 5);
        //values.put(Respuesta, p5_4_1_1);     ;
        //db.insert(TABLE_ECTRESPUESTA, null, values);
        //values.put(PreguntaID, 5);
        //values.put(Respuesta, p5_4_1_2);     ;
        //b.insert(TABLE_ECTRESPUESTA, null, values);

        values.put(PreguntaID, 5);
        values.put(Respuesta, p5_5_1_1);     ;
        long respuesta_id_55 = db.insert(TABLE_ECTRESPUESTA, null, values);
        insertDetalle_5_5(respuesta_id_55, survey_id, p5_5_1_2 );

        //values.put(PreguntaID, 5);
        //values.put(Respuesta, p5_5_1_1);     ;
        //db.insert(TABLE_ECTRESPUESTA, null, values);
        //values.put(PreguntaID, 5);
        //values.put(Respuesta, p5_5_1_2);     ;
        //db.insert(TABLE_ECTRESPUESTA, null, values);

        values.put(PreguntaID, 6);
        values.put(Respuesta, p6);     ;
        db.insert(TABLE_ECTRESPUESTA, null, values);
        db.close();
    }

    public void insertDetalle_1(long respuestaId, long survey_id, String ps1_1_1, String ps1_1_2, String ps1_1_3, String ps1_1_4 ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (ps1_1_1 !=null && !ps1_1_1.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_1_1);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_1_2 !=null && !ps1_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_1_3 !=null && !ps1_1_3.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_1_3);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_1_4 !=null && !ps1_1_4.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_1_4);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_2(long respuestaId, long survey_id, String ps1_2_1, String ps1_2_2, String ps1_2_3, String ps1_2_4 ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (ps1_2_1 !=null && !ps1_2_1.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_2_1);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
        if (ps1_2_2 !=null && !ps1_2_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_2_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_2_3 !=null && !ps1_2_3.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_2_3);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
        if (ps1_2_4 !=null && !ps1_2_4.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_2_4);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_3(long respuestaId, long survey_id, String ps1_3_1, String ps1_3_2, String ps1_3_3, String ps1_3_4 ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (ps1_3_1 !=null && !ps1_3_1.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_3_1);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_3_2 !=null && !ps1_3_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_3_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_3_3 !=null && !ps1_3_3.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_3_3);
            db.insert(TABLE_ECTDETALLE, null, values);
        }

        if (ps1_3_4 !=null && !ps1_3_4.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 1);
            values.put(Respuesta, ps1_3_4);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_5_1(long respuestaId, long survey_id, String p5_1_1_2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (p5_1_1_2 !=null && !p5_1_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 5);
            values.put(Respuesta, p5_1_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_5_2(long respuestaId, long survey_id, String p5_2_1_2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (p5_2_1_2 !=null && !p5_2_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 5);
            values.put(Respuesta, p5_2_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_5_3(long respuestaId, long survey_id, String p5_3_1_2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (p5_3_1_2 !=null && !p5_3_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 5);
            values.put(Respuesta, p5_3_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_5_4(long respuestaId, long survey_id, String p5_4_1_2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (p5_4_1_2 !=null && !p5_4_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 5);
            values.put(Respuesta, p5_4_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void insertDetalle_5_5(long respuestaId, long survey_id, String p5_5_1_2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (p5_5_1_2 !=null && !p5_5_1_2.isEmpty()) {
            values.put(RespuestaID, respuestaId);
            values.put(EncuestaID, survey_id);
            values.put(PreguntaID, 5);
            values.put(Respuesta, p5_5_1_2);
            db.insert(TABLE_ECTDETALLE, null, values);
        }
    }

    public void DeleteData(){
        System.out.println(" -------------------- BORRAMOS ENCUESTAS -------------------------");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_ENCUESTAS);
        db.close(); // Closing database connection
    }

    public void DeleteDataSuc(){
        System.out.println(" -------------------- BORRAMOS ENCUESTAS -------------------------");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_ECTSUCURSAL);
        db.close(); // Closing database connection
    }

    public void DeleteDataCol(){
        System.out.println(" -------------------- BORRAMOS ENCUESTAS -------------------------");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_ECTCOLONIA);
        db.close(); // Closing database connection
    }


    public void DeleteSingleData(int keyID){
        System.out.println(" -------------------- BORRAMOS INFORMACION ESPECIFICA -------------------------");
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_ENCUESTAS + " WHERE " + ID + " = " + keyID);
        db.close(); // Closing database connection
    }

    public List<String> getAllBranchs(){
        List<String> branchs = new ArrayList<String>();

        /*String selectQuery = "SELECT * FROM " + TABLE_ECTSUCURSAL + " ORDER BY " + SUCURSAL + " DESC";*/
        String selectQuery = "SELECT * FROM " + TABLE_ECTSUCURSAL;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                branchs.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return branchs;
    }

    public List<String> getAllColonies(String branchSelection){
        List<String> colonies = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_ECTCOLONIA + " WHERE " + SUCURSAL + " = '" + branchSelection +"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                colonies.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();
        return colonies;
    }

    public List<Surveys> getAllSurveys() {
        List<Surveys> surveysList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ENCUESTAS + " ORDER BY " + SURVEY_USUARIO + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Surveys surveys = new Surveys();
                surveys.setsurvey_id(Integer.parseInt(cursor.getString(0)));
                surveys.setsurvey_usuario(cursor.getString(1));
                surveys.setsurvey_sucursal(cursor.getString(2));
                surveys.setsurvey_cliente(cursor.getString(3));
                surveys.setsurvey_colonia(cursor.getString(4));
                surveys.setsurvey_oficio(cursor.getString(5));
                surveys.setsurvey_genero(cursor.getString(6));
                surveys.setsurvey_edad(cursor.getString(7));
                surveys.setsurvey_p1(cursor.getString(8));
                surveys.setsurvey_p2(cursor.getString(9));
                surveys.setsurvey_p3(cursor.getString(10));
                surveys.setsurvey_p4(cursor.getString(11));
                surveys.setsurvey_p5(cursor.getString(12));
                surveys.setsurvey_p6(cursor.getString(13));
                surveys.setsurvey_p7(cursor.getString(14));
                surveys.setsurvey_p8(cursor.getString(15));
                surveys.setsurvey_p9(cursor.getString(16));
                surveys.setsurvey_p10(cursor.getString(17));
                surveys.setsurvey_p11(cursor.getString(18));
                surveys.setsurvey_p12(cursor.getString(19));
                surveys.setsurvey_p13(cursor.getString(20));
                surveys.setsurvey_p14(cursor.getString(21));
                surveys.setsurvey_p15(cursor.getString(22));
                surveys.setsurvey_p16(cursor.getString(23));
                surveys.setsurvey_email(cursor.getString(24));
                surveys.setsurvey_fecha(cursor.getString(25));
                surveys.setsurvey_timer(cursor.getString(26));
                surveysList.add(surveys);
            } while (cursor.moveToNext());
        }

        // return contact list
        return surveysList;
    }


    public Surveys getInfoSurvey(int idSurvey) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ENCUESTAS, new String[] { ID,
                        SURVEY_USUARIO, SURVEY_SUCURSAL, SURVEY_CLIENTE, SURVEY_COLONIA, SURVEY_OFICIO, SURVEY_GENERO, SURVEY_EDAD,
                        SURVEY_P1, SURVEY_P2, SURVEY_P3, SURVEY_P4, SURVEY_P5, SURVEY_P6, SURVEY_P7, SURVEY_P8, SURVEY_P9, SURVEY_P10,
                        SURVEY_P11, SURVEY_P12, SURVEY_P13, SURVEY_P14, SURVEY_P15, SURVEY_P16, SURVEY_EMAIL, SURVEY_FECHA, SURVEY_TIMER}, ID + "=?",
                new String[] { String.valueOf(idSurvey) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Surveys surveys = new Surveys(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4),cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8),
                cursor.getString(9), cursor.getString(10), cursor.getString(11),cursor.getString(12), cursor.getString(13), cursor.getString(14),
                cursor.getString(15), cursor.getString(16), cursor.getString(17), cursor.getString(18), cursor.getString(19), cursor.getString(20),
                cursor.getString(21), cursor.getString(22), cursor.getString(23), cursor.getString(24), cursor.getString(25), cursor.getString(26));
        return surveys;
    }

    public List<SurveyTop> getAllSurveysTop() {
        List<SurveyTop> surveysListTop = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TOP + " ORDER BY " + SURVEY_USUARIO + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SurveyTop surveysTop = new SurveyTop();
                surveysTop.setsurvey_id(Integer.parseInt(cursor.getString(0)));
                surveysTop.setsurvey_usuario(cursor.getString(1));
                surveysTop.setsurvey_sucursal(cursor.getString(2));
                surveysTop.setsurvey_cp(cursor.getString(3));
                surveysTop.setsurvey_colonia(cursor.getString(4));
                surveysTop.setsurvey_oficio(cursor.getString(5));
                surveysTop.setsurvey_genero(cursor.getString(6));
                surveysTop.setsurvey_edad(cursor.getString(7));
                surveysTop.setsurvey_fecha(cursor.getString(14));
                surveysTop.setsurvey_timer(cursor.getString(15));
                surveysListTop.add(surveysTop);
            } while (cursor.moveToNext());
        }
        // return contact list
        return surveysListTop;
    }

    public List<SurveyRespuestas> getAllSurveysRespuestas() {
        List<SurveyRespuestas> surveysListRespuestas = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ECTRESPUESTA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SurveyRespuestas surveysRespuestas = new SurveyRespuestas();
                surveysRespuestas.setrespuesta_id(Integer.parseInt(cursor.getString(0)));
                surveysRespuestas.setpregunta_id(Integer.parseInt(cursor.getString(1)));
                surveysRespuestas.setsurvey_id(Integer.parseInt(cursor.getString(2)));
                surveysRespuestas.setrespuesta(cursor.getString(3));
                surveysRespuestas.setrespuesta_opinion(cursor.getString(4));
                surveysListRespuestas.add(surveysRespuestas);
            } while (cursor.moveToNext());
        }
        // return contact list
        return surveysListRespuestas;
    }

    public List<SurveyDetalle> getAllSurveysDetalle() {
        List<SurveyDetalle> surveysListDetalle = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ECTDETALLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SurveyDetalle surveysDetalle = new SurveyDetalle();
                surveysDetalle.setdetalle_id(Integer.parseInt(cursor.getString(0)));
                surveysDetalle.setrespuesta_id(Integer.parseInt(cursor.getString(1)));
                surveysDetalle.setpregunta_id(Integer.parseInt(cursor.getString(2)));
                surveysDetalle.setsurvey_id(Integer.parseInt(cursor.getString(3)));
                surveysDetalle.setrespuesta(cursor.getString(4));
                surveysListDetalle.add(surveysDetalle);
            } while (cursor.moveToNext());
        }
        // return contact list
        return surveysListDetalle;
    }

    public long getExitSurveyCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_ENCUESTAS);
        System.out.println("CONTEO SALIDA");
        System.out.println(count);
        db.close();
        return count;
    }

    public long getTopSurveyCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_TOP);
        System.out.println("CONTEO TOP");
        System.out.println(count);
        db.close();
        return count;
    }
}
