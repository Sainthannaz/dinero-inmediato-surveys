package mx.com.dineroinmediato.surveys.models;

public class Surveys {
    int survey_id;
    String survey_usuario, survey_sucursal, survey_cliente, survey_colonia, survey_oficio, survey_genero, survey_edad,
    p1, p2, p3,p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, survey_email, survey_fecha, survey_timer;
    public Surveys(){

    }

    public Surveys(int survey_id, String survey_usuario, String survey_sucursal, String survey_cliente, String survey_colonia,
                   String survey_oficio, String survey_genero, String survey_edad, String p1, String p2, String p3, String p4,
                   String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13, String p14,
                   String p15, String p16, String survey_email, String survey_fecha, String survey_timer ){

        this.survey_id = survey_id;
        this.survey_usuario = survey_usuario;
        this.survey_sucursal = survey_sucursal;
        this.survey_cliente = survey_cliente;
        this.survey_colonia = survey_colonia;
        this.survey_oficio = survey_oficio;
        this.survey_genero = survey_genero;
        this.survey_edad = survey_edad;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
        this.p6 = p6;
        this.p7 = p7;
        this.p8 = p8;
        this.p9 = p9;
        this.p10 = p10;
        this.p11 = p11;
        this.p12 = p12;
        this.p13 = p13;
        this.p14 = p14;
        this.p15 = p15;
        this.p16 = p16;
        this.survey_email = survey_email;
        this.survey_fecha = survey_fecha;
        this.survey_timer = survey_timer;
    }

    public int getsurvey_id() {
        return survey_id;
    }
    public void setsurvey_id(int id) {
        this.survey_id = id;
    }

    public String getsurvey_usuario() {
        return survey_usuario;
    }
    public void setsurvey_usuario(String usuario) {
        this.survey_usuario = usuario;
    }

    public String getsurvey_sucursal() {
        return survey_sucursal;
    }
    public void setsurvey_sucursal(String sucursal) {
        this.survey_sucursal = sucursal;
    }

    public String getsurvey_cliente() {
        return survey_cliente;
    }
    public void setsurvey_cliente(String cliente) {
        this.survey_cliente = cliente;
    }

    public String getsurvey_colonia() {
        return survey_colonia;
    }
    public void setsurvey_colonia(String colonia) {
        this.survey_colonia = colonia;
    }

    public String getsurvey_oficio() {
        return survey_oficio;
    }
    public void setsurvey_oficio(String oficio) {
        this.survey_oficio = oficio;
    }

    public String getsurvey_genero() {
        return survey_genero;
    }
    public void setsurvey_genero(String genero) {
        this.survey_genero = genero;
    }

    public String getsurvey_edad() {
        return survey_edad;
    }
    public void setsurvey_edad(String edad) {
        this.survey_edad = edad;
    }

    public String getsurvey_p1() {
        return p1;
    }
    public void setsurvey_p1(String p1) {
        this.p1 = p1;
    }

    public String getsurvey_p2() {
        return p2;
    }
    public void setsurvey_p2(String p2) {
        this.p2 = p2;
    }

    public String getsurvey_p3() {
        return p3;
    }
    public void setsurvey_p3(String p3) {
        this.p3 = p3;
    }

    public String getsurvey_p4() {
        return p4;
    }
    public void setsurvey_p4(String p4) {
        this.p4 = p4;
    }

    public String getsurvey_p5() {
        return p5;
    }
    public void setsurvey_p5(String p5) {
        this.p5 = p5;
    }

    public String getsurvey_p6() {
        return p6;
    }
    public void setsurvey_p6(String p6) {
        this.p6 = p6;
    }

    public String getsurvey_p7() {
        return p7;
    }
    public void setsurvey_p7(String p7) {
        this.p7 = p7;
    }

    public String getsurvey_p8() {
        return p8;
    }
    public void setsurvey_p8(String p8) {
        this.p8 = p8;
    }

    public String getsurvey_p9() {
        return p9;
    }
    public void setsurvey_p9(String p9) {
        this.p9 = p9;
    }

    public String getsurvey_p10() {
        return p10;
    }
    public void setsurvey_p10(String p10) {
        this.p10= p10;
    }

    public String getsurvey_p11() {
        return p11;
    }
    public void setsurvey_p11(String p11) {
        this.p11 = p11;
    }

    public String getsurvey_p12() {
        return p12;
    }
    public void setsurvey_p12(String p12) {
        this.p12 = p12;
    }

    public String getsurvey_p13() {
        return p13;
    }
    public void setsurvey_p13(String p13) {
        this.p13 = p13;
    }

    public String getsurvey_p14() {
        return p14;
    }
    public void setsurvey_p14(String p14) {
        this.p14 = p14;
    }

    public String getsurvey_p15() {
        return p15;
    }
    public void setsurvey_p15(String p15) {
        this.p15 = p15;
    }

    public String getsurvey_p16() {
        return p16;
    }
    public void setsurvey_p16(String p16) {
        this.p16 = p16;
    }

    public String getsurvey_email() {
        return survey_email;
    }
    public void setsurvey_email(String email) {
        this.survey_email = email;
    }

    public String getsurvey_fecha() {
        return survey_fecha;
    }
    public void setsurvey_fecha(String fecha) {
        this.survey_fecha = fecha;
    }

    public String getsurvey_timer() {
        return survey_timer;
    }
    public void setsurvey_timer(String timer) {
        this.survey_timer = timer;
    }

}
