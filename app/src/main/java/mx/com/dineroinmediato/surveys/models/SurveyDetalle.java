package mx.com.dineroinmediato.surveys.models;

public class SurveyDetalle {
    int detalle_id, respuesta_id, pregunta_id, survey_id;
    String respuesta;
    public SurveyDetalle(){

    }

    public SurveyDetalle(int detalle_id, int respuesta_id, int pregunta_id, int survey_id, String respuesta){
        this.detalle_id = detalle_id;
        this. respuesta_id = respuesta_id;
        this.pregunta_id = pregunta_id;
        this. survey_id = survey_id;
        this.respuesta = respuesta;
    }

    public int getdetalle_id() {
        return detalle_id;
    }
    public void setdetalle_id(int id) {
        this.detalle_id = id;
    }

    public int getrespuesta_id() {
        return respuesta_id;
    }
    public void setrespuesta_id(int id) {
        this.respuesta_id = id;
    }

    public int getpregunta_id() {
        return pregunta_id;
    }
    public void setpregunta_id(int id) {
        this.pregunta_id = id;
    }

    public int getsurvey_id() {
        return survey_id;
    }
    public void setsurvey_id(int id) {
        this.survey_id = id;
    }

    public String getrespuesta() {
        return respuesta;
    }
    public void setrespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
