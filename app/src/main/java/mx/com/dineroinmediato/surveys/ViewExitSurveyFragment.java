package mx.com.dineroinmediato.surveys;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mx.com.dineroinmediato.surveys.models.SurveyDetalle;
import mx.com.dineroinmediato.surveys.models.SurveyRespuestas;
import mx.com.dineroinmediato.surveys.models.SurveyTop;
import mx.com.dineroinmediato.surveys.models.Surveys;
import mx.com.dineroinmediato.surveys.tools.DatabaseHandler;
import mx.com.dineroinmediato.surveys.tools.DividerItemDecoration;
import mx.com.dineroinmediato.surveys.tools.PermissionUtils;
import mx.com.dineroinmediato.surveys.tools.SessionManager;

public class ViewExitSurveyFragment extends Fragment {

    Button btnCancelar, btnGuardar;
    private SessionManager session;
    private ProgressDialog pDialog;
    String userEmail, userSucursal;
    DatabaseHandler db;
    private List<Surveys> surveysList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ExitSurveyAdapter mAdapter;
    public Button deleteDB, uploadDB, exportDB, exportDBTop;
    private SwipeRefreshLayout refreshLayout;
    private static final int MY_READ_WRITE_PERMISSION_REQUEST_CODE = 1;
    private static final int READ_WRITE_PERMISSION_REQUEST_CODE = 2;
    public ViewExitSurveyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.survey_view_exit_survey, null);
        db = new DatabaseHandler(getActivity());
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        exportDB = view.findViewById(R.id.btnExport);
        exportDBTop = view.findViewById(R.id.btnTop);
        deleteDB = view.findViewById(R.id.btnDelete);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL_LIST, 0));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        List<Surveys> surveys = db.getAllSurveys();
        mAdapter = new ExitSurveyAdapter(getContext(), surveys);
        recyclerView.setAdapter(mAdapter);
        requestReadWritePermission(MY_READ_WRITE_PERMISSION_REQUEST_CODE);
        refreshLayout = view.findViewById(R.id.swipeRefresh);
        refreshLayout.setColorSchemeResources(
                R.color.s1,
                R.color.s2,
                R.color.s3,
                R.color.s4
        );

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                        refreshLayout.setRefreshing(false);
                    }
                }
        );

        exportDB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog builder = new AlertDialog.Builder(getActivity())
                        .setCancelable(true)
                        .setIcon(R.drawable.ic_info)
                        .setTitle("Exportar DB")
                        .setMessage("¡Se exportara la DB a CSV!")
                        .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                pDialog.setMessage("Generando CSV...");
                                showDialog();
                                createCSV();
                                hideDialog();
                                Toast.makeText(getContext(), getString(R.string.success_csv),
                                        Toast.LENGTH_LONG).show();
                            }
                        }).show();
                refreshData();
            }
        });

        exportDBTop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                AlertDialog builder = new AlertDialog.Builder(getActivity())
                        .setCancelable(true)
                        .setIcon(R.drawable.ic_info)
                        .setTitle("Exportar DB Top")
                        .setMessage("¡Se exportara la DB a CSV!")
                        .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                pDialog.setMessage("Generando CSV...");
                                showDialog();
                                createCSVTOP();
                                hideDialog();
                                Toast.makeText(getContext(), getString(R.string.success_csv),
                                        Toast.LENGTH_LONG).show();
                            }
                        }).show();
                refreshData();

            }
        });

        deleteDB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                deleteDialog();
            }
        });

        return view;
    }



    public void deleteDialog(){
        AlertDialog builder = new AlertDialog.Builder(getActivity())
                .setCancelable(true)
                .setIcon(R.drawable.ic_info)
                .setTitle("Eliminar BD local")
                .setMessage("¿Desea eliminar la BD local? todos los datos se perderan")
                .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        pDialog.setMessage("Eliminando BD...");
                        showDialog();
                        db.DeleteData();
                        hideDialog();
                        refreshData();
                        Toast.makeText(getContext(), getString(R.string.success_erase),
                                Toast.LENGTH_LONG).show();
                    }
                }).show();
    }

    public void refreshData(){
        List<Surveys> surveys = db.getAllSurveys();
        mAdapter = new ExitSurveyAdapter(getContext(), surveys);
        recyclerView.setAdapter(mAdapter);
    }

    public void createCSV(){
        List<Surveys> list = new ArrayList<Surveys>();

        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        list = db.getAllSurveys();
        System.out.println(list);
        if(list.size() <= 0) {
            Toast.makeText(getContext(), getString(R.string.nodata_error),
                    Toast.LENGTH_LONG).show();
        } else {
            System.out.println(list);
            File exportDir = new File(Environment.getExternalStorageDirectory(), "encuestas_salida"); //

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            String fileName = "src/main/resources/test.csv";

            File file = new File(exportDir, "Surveys_"+ timeStamp +".csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                String columnsData[] = {"ID", "Usuario", "Sucursal", "Cliente", "Colonia", "Oficio", "Genero", "Edad", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "Email", "Fecha", "Tiempo"};
                csvWrite.writeNext(columnsData);
                System.out.println("Tamaño de la lista:" + list.size());
                for (int index = 0; index < list.size(); index++) {
                    int id = list.get(index).getsurvey_id();
                    String user = list.get(index).getsurvey_usuario();
                    String sucursal = list.get(index).getsurvey_sucursal();
                    String cliente = list.get(index).getsurvey_cliente();
                    String colonia = list.get(index).getsurvey_colonia();
                    String oficio = list.get(index).getsurvey_oficio();
                    String genero = list.get(index).getsurvey_genero();
                    String edad = list.get(index).getsurvey_edad();
                    String p1 = list.get(index).getsurvey_p1();
                    String p2 = list.get(index).getsurvey_p2();
                    String p3 = list.get(index).getsurvey_p3();
                    String p4 = list.get(index).getsurvey_p4();
                    String p5 = list.get(index).getsurvey_p5();
                    String p6 = list.get(index).getsurvey_p6();
                    String p7 = list.get(index).getsurvey_p7();
                    String p8 = list.get(index).getsurvey_p8();
                    String p9 = list.get(index).getsurvey_p9();
                    String p10 = list.get(index).getsurvey_p10();
                    String p11 = list.get(index).getsurvey_p11();
                    String p12 = list.get(index).getsurvey_p12();
                    String p13 = list.get(index).getsurvey_p13();
                    String p14 = list.get(index).getsurvey_p14();
                    String p15 = list.get(index).getsurvey_p15();
                    String p16 = list.get(index).getsurvey_p16();
                    String email = list.get(index).getsurvey_email();
                    String fecha = list.get(index).getsurvey_fecha();
                    String tiempo = list.get(index).getsurvey_timer();
                    System.out.println(user);
                    System.out.println(sucursal);
                    System.out.println(genero);
                    System.out.println(edad);
                    System.out.println(p1);
                    String surveysData[] = {String.valueOf(id), user, sucursal, cliente, colonia, oficio, genero, edad, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, email, fecha, tiempo};
                    csvWrite.writeNext(surveysData);
                }
                csvWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void createCSVTOP(){
        List<SurveyTop> list = new ArrayList<SurveyTop>();
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        list = db.getAllSurveysTop();
        System.out.println(list);
        if(list.size() <= 0) {
            Toast.makeText(getContext(), getString(R.string.nodata_error),
                    Toast.LENGTH_LONG).show();
        } else {
            System.out.println(list);
            File exportDir = new File(Environment.getExternalStorageDirectory(), "encuestas_top"); //

            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            //String fileName = "src/main/resources/test.csv";

            File file = new File(exportDir, "SurveysTOP_Header_"+ timeStamp +".csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                String columnsData[] = {"ID", "Usuario", "Sucursal", "CP", "Colonia", "Oficio", "Genero", "Edad", "Fecha", "Tiempo"};
                csvWrite.writeNext(columnsData);
                System.out.println("Tamaño de la lista:" + list.size());
                for (int index = 0; index < list.size(); index++) {
                    int id = list.get(index).getsurvey_id();
                    String user = list.get(index).getsurvey_usuario();
                    String sucursal = list.get(index).getsurvey_sucursal();
                    String cp = list.get(index).getsurvey_cp();
                    String colonia = list.get(index).getsurvey_colonia();
                    String oficio = list.get(index).getsurvey_oficio();
                    String genero = list.get(index).getsurvey_genero();
                    String edad = list.get(index).getsurvey_edad();
                    String fecha = list.get(index).getsurvey_fecha();
                    String tiempo = list.get(index).getsurvey_timer();
                    System.out.println(user);
                    System.out.println(sucursal);
                    System.out.println(genero);
                    System.out.println(edad);
                    String surveysData[] = {String.valueOf(id), user, sucursal, cp, colonia, oficio, genero, edad, fecha, tiempo};
                    csvWrite.writeNext(surveysData);
                }
                csvWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<SurveyRespuestas> listRespuestas = new ArrayList<SurveyRespuestas>();
        listRespuestas = db.getAllSurveysRespuestas();
        if(listRespuestas.size() <= 0) {
            Toast.makeText(getContext(), getString(R.string.nodata_error),
                    Toast.LENGTH_LONG).show();
        } else {
            File exportDir = new File(Environment.getExternalStorageDirectory(), "encuestas_top");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            //String fileName = "src/main/resources/test.csv";

            File file = new File(exportDir, "SurveysTop_Respuestas"+ timeStamp +".csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                String columnsData[] = {"ID_Respuesta", "ID_Pregunta", "ID_Survey", "Respuesta", "RespOpinion"};
                csvWrite.writeNext(columnsData);
                for (int index = 0; index < listRespuestas.size(); index++) {
                    int respuesta_id = listRespuestas.get(index).getrespuesta_id();
                    int pregunta_id = listRespuestas.get(index).getpregunta_id();
                    int survey_id = listRespuestas.get(index).getsurvey_id();
                    String respuesta = listRespuestas.get(index).getrespuesta();
                    String respuesta_opinion = listRespuestas.get(index).getrespuesta_opinion();
                    String surveysData[] = {String.valueOf(respuesta_id), String.valueOf(pregunta_id), String.valueOf(survey_id), respuesta, respuesta_opinion};
                    csvWrite.writeNext(surveysData);
                }
                csvWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<SurveyDetalle> listDetalle = new ArrayList<SurveyDetalle>();
        listDetalle = db.getAllSurveysDetalle();
        if(listDetalle.size() <= 0) {
            Toast.makeText(getContext(), getString(R.string.nodata_error),
                    Toast.LENGTH_LONG).show();
        } else {
            File exportDir = new File(Environment.getExternalStorageDirectory(), "encuestas_top");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            //String fileName = "src/main/resources/test.csv";

            File file = new File(exportDir, "SurveysTop_Detalle"+ timeStamp +".csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                String columnsData[] = {"ID_Detalle", "ID_Respuesta", "ID_Pregunta", "ID_Survey", "Respuesta"};
                csvWrite.writeNext(columnsData);
                for (int index = 0; index < listDetalle.size(); index++) {
                    int detalle_id = listDetalle.get(index).getrespuesta_id();
                    int respuesta_id = listDetalle.get(index).getrespuesta_id();
                    int pregunta_id = listDetalle.get(index).getpregunta_id();
                    int survey_id = listDetalle.get(index).getsurvey_id();
                    String respuesta = listDetalle.get(index).getrespuesta();
                    String surveysData[] = {String.valueOf(detalle_id), String.valueOf(respuesta_id), String.valueOf(pregunta_id), String.valueOf(survey_id), respuesta};
                    csvWrite.writeNext(surveysData);
                }
                csvWrite.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void requestReadWritePermission(int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog
                    .newInstance(requestCode, false).show(
                    getFragmentManager(), "dialog");
        } else {
            // Location permission has not been granted yet, request it.
            PermissionUtils.requestPermission((AppCompatActivity) getContext(), requestCode,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, false);
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == READ_WRITE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location button if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {

            }
        } else if (requestCode == READ_WRITE_PERMISSION_REQUEST_CODE) {
            // Enable the My Location layer if the permission has been granted.
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }

            } else {
                //
            }
        }
    }

    

}
