[33mcommit 361f5c77948c3d89f6eb5f1398ec17496d24cbe0[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31msupervision/master[m[33m, [m[1;31morigin/master[m[33m)[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Mon Sep 17 14:09:33 2018 -0500

    Reset cronometro

[33mcommit 87132f66b003f7bd1ead263df762176428d29ec7[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Mon Sep 17 12:38:29 2018 -0500

    Se termina la nueva estructura, resta integrar el guardado y controles de llenado, se añade animación a boton

[33mcommit 23e19f1bf7a0b2adf977b0e0402bb58206615f4f[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Mon Sep 17 00:50:35 2018 -0500

    Se agrega funcion para exportar a CSV, se agrega boton de eliminar BD local, se hacen cambios en el layout de encuesta de salida

[33mcommit 7b5f5e4f4c1e49e1a612ba798349793aed469f27[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Sat Sep 15 15:00:28 2018 -0500

    Se termina el adaptardor y modelo de encuestas

[33mcommit 7d8e40430b7606e1a51b741fb0e9a97d7919489e[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Sat Sep 15 12:58:10 2018 -0500

    la vista ya presenta datos

[33mcommit 9692efc5b5078970733d77f33e348fde5ba55fcb[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Fri Sep 14 17:11:14 2018 -0500

    se agrega la vista de encuestas de salida, se esta agregando metodo para obtener los datos de cada encuesta seleccionada

[33mcommit d61050f89e5248f0dc1ffcecd03609b02b9d5b14[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Fri Sep 14 10:39:33 2018 -0500

    ?

[33mcommit 22bc4d0d6db12bacfc613f2da75b0860b38a6398[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Thu Sep 13 15:36:27 2018 -0500

    Se agrega FAB y metodo para iniciar la encuesta

[33mcommit cf599a8324cf4f10eaf47d059834bd5766a20cad[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Thu Sep 13 14:57:55 2018 -0500

    se agrega a la vista de encuestas un submenu para ver o eliminar la encuesta, cambios en estetica

[33mcommit 875e349db04ab8c0935547f8a1ae29a8ea627fd0[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Tue Sep 11 16:17:28 2018 -0500

    Se agrega un recycler para poder ver los datos de las encuestas realizadas

[33mcommit 6d53fd7aa2ef2ae187cfeb37b4283e281b5e2f8a[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Thu Sep 6 23:35:44 2018 -0500

    SQLITE

[33mcommit 5176716dceff7d07991ce1cd157a666c3e71b629[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Sat Aug 25 10:38:05 2018 -0500

    Se agrega la encuesta top of mind

[33mcommit 12228ed67788c0f99bd7c7995e37fef109cc2fa4[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Fri Aug 24 15:22:52 2018 -0500

    Se agrega encuesta top of mind

[33mcommit e31543b6eb27100abbca26e87893e4b8e969e31a[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Fri Aug 24 00:19:03 2018 -0500

    Se agregan los cambios a la primer encuesta

[33mcommit 9743b0097d240f8d1cd1520eabd0c5079a82a647[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Thu Aug 16 00:10:04 2018 -0500

    se agregan las actividades de las encuestas, se agrega control de inicion de sesion y se agrega informacion al session maganer, se agregan los host y se agrega el manejador de inicio de sesion

[33mcommit 4501ed3004e4c82aca15826ac78b14db56d7ee9e[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Thu Aug 9 14:17:15 2018 -0500

    fragments de navegacion agregados

[33mcommit fb41760869647649c7d37af7905213af9e09ac0a[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Fri Aug 3 11:55:42 2018 -0500

    Se agrega el administrador de inicio de sesión de usuarios, manejador de permisos de versiones Android, menu drawer del menu principal

[33mcommit d4a9e904839cc8dfee21d614be019ac312861523[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Wed Aug 1 11:28:42 2018 -0500

    Se agrega la vista de login y se crean validadores de campos

[33mcommit 0d1dcfbc01973168dc82185841a46c1f5d5aecba[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Tue Jul 31 14:41:22 2018 -0500

    Se agregan las librerias de soporte para android Oreo del proyecto

[33mcommit 1edd351251630882874740094377631813363e7c[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Tue Jul 31 13:57:47 2018 -0500

    Se agrega splash screen y estilo de la app

[33mcommit 9c9e4ec998166e5fe49fb8317caa8c290f8545c4[m
Author: Luis Gabriel Morales Taylor <lmorales@rebelbot.mx>
Date:   Tue Jul 31 12:43:53 2018 -0500

    Codigo inicial
